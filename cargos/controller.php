<?php
require_once('constants.php');
require_once('model.php');
require_once('view.php');

function handler() {
	$event = VIEW_REPORT;
	$uri = $_SERVER['REQUEST_URI'];
	$peticiones = array(
			ADD,
			SET,
			GET,
			DELETE,
			EDIT,
			REPORT,
			VIEW_ADD,
			VIEW_SET,
			VIEW_GET,
			VIEW_DELETE,
			VIEW_REPORT,
			VIEW_EDIT);
	foreach ($peticiones as $peticion) {
		$uri_peticion = MODULO.'/'.$peticion.'/';
		if( strpos($uri, $uri_peticion) == true ) {
			$event = $peticion;
		}
	}

	$data = helper_data();
	$cargo = set_obj();
	$cargo->estado = $data['estado'];
	switch ($event) {
		case ADD:
			$data = array(
			'nombre_cargo'=>'',
			'tipo_asig_area'=>1,
			'estado'=>$cargo->estado
			);
			retornar_vista(VIEW_ADD, $data, '', $cargo->estado);
			break;
		case SET:
			$cargo->set($data);
			$msg = $cargo->mensaje;
			if (strpos($msg, 'Error') === false) {
				$sqlWhere = sprintf("WHERE estado = '%s'", $data['estado']);
				$data = $cargo->getAll($sqlWhere);
				retornar_vista(VIEW_REPORT, $data, $msg, $cargo->estado);
			} else {
				retornar_vista(VIEW_ADD, $data, $msg);
			}
			break;
		case GET:
			$cargo->get($data['id_cargo']);
			$data = array(
					'nombre_cargo'=>$cargo->nombre_cargo,
					'tipo_asig_area'=>$cargo->tipo_asig_area,
					'id_cargo'=>$cargo->id_cargo
			);
			retornar_vista(VIEW_GET, $data, $cargo->mensaje, $cargo->estado);
			break;
		case DELETE:
			$cargo->delete($data['id_cargo'], $data['estado']);
			$msg = $cargo->mensaje;
			$sqlWhere = sprintf("WHERE estado = %s", $data['estado']);
			$data = $cargo->getAll($sqlWhere);
			retornar_vista(VIEW_REPORT, $data, $msg, $cargo->estado);
			break;
		case EDIT:
			$cargo->edit($data);
			$sqlWhere = sprintf("WHERE estado = %s", $data['estado']);
			$msg = $cargo->mensaje;
			if (strpos($msg, 'Error') === false) {
				$data = $cargo->getAll($sqlWhere);
				retornar_vista(VIEW_REPORT, $data, $msg, $cargo->estado);
			} else {
				retornar_vista(VIEW_EDIT, array(), $msg);
			}
			break;
		case REPORT:
			$sqlWhere = sprintf("WHERE estado = '%s'", $data['estado']);
			$data = $cargo->getAll($sqlWhere);
			retornar_vista(VIEW_REPORT, $data, $cargo->mensaje, $cargo->estado);
			break;
		default:
			retornar_vista($event);
	}
}

function set_obj() {
	$obj = new Cargo();
	return $obj;
}

function helper_data() {
	$data = array();
	$data['estado'] = true;

	if($_POST) {
		if(array_key_exists('id_cargo', $_POST))
			$data['id_cargo'] = htmlentities($_POST['id_cargo']);
		if(array_key_exists('nombre_cargo', $_POST))
			$data['nombre_cargo'] = htmlentities($_POST['nombre_cargo']);
		if(array_key_exists('tipo_asig_area', $_POST))
			$data['tipo_asig_area'] = htmlentities($_POST['tipo_asig_area']);		
		if(array_key_exists('estado', $_POST))
			$data['estado'] = htmlentities($_POST['estado']);
	} else if($_GET) {
		if(array_key_exists('id_cargo', $_GET))
			$data['id_cargo'] = htmlentities($_GET['id_cargo']);
		if(array_key_exists('estado', $_GET))
			$data['estado'] =  htmlentities($_GET['estado']);
	}

	return $data;
}

handler();

?>
