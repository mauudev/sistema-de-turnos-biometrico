<?php

require_once('../core/db_abstract_model.php');

class Cargo extends DBAbstractModel{

	public $nombre_cargo;
	public $tipo_asig_area;
	public $estado;
	public $id_cargo;

	function __construct(){
		$this->db_name = 'ctrl_asis';
	}

	function __destruct() {
		unset($this);
	}
	# Traer datos de un cargo
	public function get($id_cargo = ''){
		if($id_cargo != '') {
			$this->query = "
			SELECT *
			FROM cargo
			WHERE id_cargo = '$id_cargo'
			";
			$this->get_results_from_query();
			if(count($this->rows) == 1) {
				foreach ($this->rows[0] as $propiedad=>$valor) {
					$this->$propiedad = $valor;
				}
				$this->mensaje = 'Cargo encontrado';
			} else {
				$this->mensaje = 'Error: Cargo no encontrado';
			}
		}
	}

// 	public function exists($sqlWhere) {
// 		$this->query = "SELECT * FROM cargo " . $sqlWhere;
// 		$this->get_results_from_query();
// 		if(count($this->rows) > 0) {
// 			$this->mensaje = 'Registros encontrados';
// 			return true;
// 		} else {
// 			$this->mensaje = 'Error: Registros no encontrados';
// 			return false;
// 		}
// 	}

	# Crear un nuevo cargo
	public function set($data=array()) {
		foreach($data as $campo => $valor)
			$$campo = $valor;

		try {
			$existsCond = sprintf("WHERE nombre_cargo='%s'", $data['nombre_cargo']);
			$idemCargos = $this->getAll($existsCond);
			if (count($idemCargos) > 0)
				throw new Exception('El nombre del cargo ya existe en los registros');
			
			$this->query = "INSERT INTO cargo
			(nombre_cargo, tipo_asig_area, estado)
			VALUES
			('$nombre_cargo', $tipo_asig_area, $estado)";
			$this->execute_single_query();
			$this->mensaje = 'Cargo registrado correctamente!';
		} catch (Exception $e) {
			$this->mensaje = sprintf("Error: Cargo no registrado (%s)", $e->getMessage());
		}
	}
	# Modificar un cargo
	public function edit($data=array()){
		if(array_key_exists('id_cargo', $data)) {
			foreach($data as $field => $value)
				$$field = $value;

			try {
				$this->query = sprintf("
						UPDATE cargo SET
						nombre_cargo='%s',
						tipo_asig_area=%u,
						estado='%s'
						WHERE id_cargo = %d",
						$nombre_cargo,
						$tipo_asig_area,
						$estado,
						$id_cargo);
				$this->execute_single_query();
				$this->mensaje = 'Cargo modificado correctamente';
			} catch (Exception $e) {
				$this->mensaje = sprintf("Error: Cargo no actualizado (%s)", $e->getMessage());
				
			}
		}
	}
	# Eliminar un cargo
	public function delete($id_cargo='', $isActive=false){
		if($id_cargo != '') {
			try {
				$this->query = sprintf("
						UPDATE cargo
						SET estado = $isActive
						WHERE id_cargo = %d",
						$id_cargo);
				$this->execute_single_query();
				$this->mensaje = sprintf('Cargo %s correctamente', $isActive ? 'activado' : 'desactivado');
			} catch (Exception $e) {
				$this->mensaje = sprintf("Error: No se pudo cambiar el estado del registro (%s)", $e->getMessage());
			}
		}
	}


	# lista todas la cargos
	public function getAll($sqlWhere = '') {
		try {
			$this->query = "SELECT * FROM cargo $sqlWhere";
			$this->get_results_from_query();
			$this->mensaje = count($this->rows) > 0 ? 'Registros recuperados con �xito' : 'No existen registros';
			return $this->rows;
		} catch (Exception $e) {
			$this->mensaje = "Error: " . $e->getMessage();
		}

	}
}

?>