<?php
$diccionario = array(
		'title'=>'Asistencia',
		'subtitle'=>array(
				VIEW_SET=>'Captura de registros del reloj biométrico',
				VIEW_GET=>'Captura de registros del reloj biométrico'
		),
		'page_info'=>array(
				VIEW_SET=>'Esta página permite recuperar los registros capturados por el reloj biométrico',
				VIEW_GET=>'Esta página permite recuperar los registros capturados por el reloj biométrico'
		),
		'form_title'=>array(
				VIEW_SET=>'Recuperación de registros desde el reloj biométrico',
				VIEW_GET=>'Recuperación de registros desde el reloj biométrico'

		),
		'links_menu'=>array(
				'VIEW_SET'=>'/webs/sis2/'.MODULO.'/'.VIEW_SET.'/',
				'VIEW_GET'=>'/webs/sis2/'.MODULO.'/'.VIEW_SET.'/',
		),
		'form_actions'=>array(
				'SET'=>'/webs/sis2/'.MODULO.'/'.SET.'/',
				'GET'=>'/webs/sis2/'.MODULO.'/'.GET.'/'
		)
);

function get_template($form='site_template') {
	$file = '../site_media/html/'.$form.'.html';
	$template = file_get_contents($file);
	return $template;
}

function render_dinamic_data($html, $data) {
	foreach ($data as $clave=>$valor) {
		$html = str_replace('{'.$clave.'}', $valor, $html);
	}
	return $html;
}

function retornar_vista($vista, $data=array(), $msg = '') {
	global $diccionario;
	$html = get_template();
	$html = str_replace('{page_content}', get_template(MODULO.'_'.$vista), $html);
	$html = str_replace('{title}', $diccionario['title'], $html);
	$html = str_replace('{subtitle}', $diccionario['subtitle'][$vista],$html);
	$html = str_replace('{page_info}', $diccionario['page_info'][$vista],$html);
	$html = str_replace('{form_title}', $diccionario['form_title'][$vista],$html);
	$html = render_dinamic_data($html, $diccionario['form_actions']);
	$html = render_dinamic_data($html, $diccionario['links_menu']);
	$html = render_dinamic_data($html, $data);

	if ($msg == '') {
		$html = str_replace('{mensaje}', "<div id='box-msg' class='albox errorbox' style='visibility: hidden'></div>", $html);
	} else {
		if (strpos($msg, 'Error') === false ) {
			$msg = sprintf("<div id='box-msg' class='albox succesbox'>%s</div>", $msg);
		} else {
			$msg = sprintf("<div id='box-msg' class='albox errorbox'>%s</div>", $msg);
		}
		$html = str_replace('{mensaje}', $msg, $html);
	}

	print $html;
}
?>