<?php
require_once('constants.php');
require_once('model.php');
require_once('view.php');

function handler() {
	$event = VIEW_GET;
	$uri = $_SERVER['REQUEST_URI'];
	$peticiones = array(SET, GET, REPORT, VIEW_SET, VIEW_GET);
	foreach ($peticiones as $peticion) {
		$uri_peticion = MODULO.'/'.$peticion.'/';
		if( strpos($uri, $uri_peticion) == true ) {
			$event = $peticion;
		}
	}
	$bioclock_data = helper_user_data();
	$bioclock = set_obj();

	switch ($event) {
		case GET:
			retornar_vista(VIEW_GET);
			break;
		case SET:
			$bioclock_data['file'] = $_FILES["datafilePath"]["tmp_name"];
			// 			print_r($bioclock_data);
			// 			echo "Upload: " . $_FILES["datafilePath"]["name"] . "<br>";
			// 			echo "Type: " . $_FILES["datafilePath"]["type"] . "<br>";
			// 			echo "Size: " . ($_FILES["datafilePath"]["size"] / 1024) . " kB<br>";
			// 			echo "Stored in: " . $_FILES["datafilePath"]["tmp_name"];
			$bioclock->getRegistros($bioclock_data);
			retornar_vista(VIEW_SET, array(), $bioclock->mensaje);
			break;
		default:
			retornar_vista($event);
	}
}

function set_obj() {
	$obj = new BioClockReader();
	return $obj;
}

function helper_user_data() {
	$bioclock_data = array();
	if($_POST) {
		if(array_key_exists('tolerance', $_POST)) {
			$bioclock_data['tolerance'] = htmlentities($_POST['tolerance']);
		}
		if(array_key_exists('datafilePath', $_POST)) {
			$bioclock_data['datafilePath'] = htmlentities($_POST['datafilePath']);
		}
	}
	return $bioclock_data;
}
handler();
?>
