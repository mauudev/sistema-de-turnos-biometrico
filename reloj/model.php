<?php

require_once('../core/time_utils.php');
require_once('../emp_turnos/model.php');
require_once('../asis_lab/model.php');
require_once('../sal_inter/model.php');
require_once('../asis_no_lab/model.php');

class ClockRegister {
	private $date;
	private $time;

	public function getDate() {
		return $this->date;
	}

	public function getTime() {
		return $this->time;
	}

	public function getDateTime() {
		return $this->date . ' ' . $this->time;
	}

	function __construct($date, $time) {
		$this->date = $date;
		$this->time = $time;
	}

	function __toString() {
		return $this->getDateTime() . '<br/>';
	}
}

class BioClockReader {
	public $mensaje;
	public $tolerance;

	public function __construct() {
	}

	# Traer registros procesados desde el archivo del reloj biometrico
	public function getRegistros($bioclock_data) {
		$this->tolerance = $bioclock_data['tolerance'];

		try {
			$registros = self::readRawBioClockData($bioclock_data['file']);

			foreach ($registros as $codEmp => $registro) {
				$empTurnosDB = new EmpTurnos();
				$turnos = $empTurnosDB->getTurnos($codEmp);
				self::filterByPeriod($codEmp, $registro, $turnos);
			}

			$this->mensaje = "Registros ingresados con �xito";
		} catch (Exception $e) {
			$this->mensaje = "Error: " . $e->getMessage();
		}
	}

	private function readRawBioClockData($file) {
		$registros = array();

		$handle = @fopen("$file", "r");

		if ($handle) {
			$pattern = "/(\d+)\t(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/i";

			$empTurnos = new EmpTurnos();

			while (($buffer = fgets($handle, 4096)) !== false) {
				if (preg_match($pattern, $buffer, $matches)) {
					//print_r($matches);

					if ($empTurnos->hasTurnos($matches[1])) {
						$date = "20$matches[2]-$matches[3]-$matches[4]";
						$time = "$matches[5]:$matches[6]:$matches[7]";
						if (isset($registros[$matches[1]])) {
							$lastReg = array_pop($registros[$matches[1]]);
							if (isset($lastReg) && TimeUtils::diffDateTime($time, $lastReg->getTime()) < $this->tolerance) {
								$registros[$matches[1]][] = new ClockRegister($date, $time);
							} else  {
								$registros[$matches[1]][] = $lastReg;
								$registros[$matches[1]][] = new ClockRegister($date, $time);
							}
						} else {
							$registros[$matches[1]] = array(new ClockRegister($date, $time));
						}
					} else {
						throw new Exception("Formato incorrecto: Empleado no registrado en el reloj biom�trico");
					}
				}
				else {
					//throw new Exception("No encontrado: $buffer $pattern");
					throw new Exception("Formato incorrecto del archivo del reloj biom�trico");
				}
			}
			// 			print_r($registros);
			// 			echo '<br/>';
		}

		if (!feof($handle)) {
			fclose($handle);
			throw new Exception("Error en la recuperacion del archivo");
		}

		return $registros;
	}

	private function filterByPeriod($codEmp, $registro, $turnos) {
		// 		echo '<hr/>' . $codEmp . '<hr/>';

		$_POST['send'] = true;
		$regIdx = 0;

		foreach ($turnos as $turno) {
			if (isset($registro[$regIdx])) {
				$asis_data = array();
				$asis_data['codEmp'] = $codEmp;
				$asis_data['fecha'] = $registro[$regIdx]->getDate();
				$asis_data['turno'] = $turno["id_turno"];
				$asis_data['hor_ent'] = null;
				$asis_data['hor_sal'] = null;
				$asis_data['observado'] = true;
				$isFinished = false;

				while (!$isFinished && isset($registro[$regIdx])) {
					$clockReg = $registro[$regIdx];

					$delayReg = TimeUtils::diffDateTime($turno["hora_ent"], $registro[$regIdx]->getTime());
					if (TimeUtils::diffDateTime($turno["hora_ent"], $clockReg->getTime()) > $turno["tol_ent"]) {
						$regIdx = self::setNoLaboralReg($codEmp, $registro, $regIdx, $turno['hora_ent'], $turno['tol_ent']);
					} else if (!isset($asis_data['hor_ent']) && $delayReg >= -$turno["tol_ent"]) {
						//Laboral assistance entry record
						$asis_data['hor_ent'] = $clockReg->getTime();
						$regIdx++;
					} else if (TimeUtils::diffDateTime($turno["hora_sal"], $clockReg->getTime()) > $turno["tol_sal"]) {
						//Intermediate out record
						$regIdx = self::setSalInterReg($codEmp, $registro, $regIdx, $turno);
					} else if (!isset($asis_data['hor_sal']) && TimeUtils::diffDateTime($turno["hora_sal"], $clockReg->getTime()) >= -$turno["tol_sal"]) {
						//Laboral assistance exit record
						$asis_data['hor_sal'] = $clockReg->getTime();
						$asis_data['observado'] = false;
						$isFinished = true;
						$regIdx++;
					} else {
						//Laboral assistance null exit record
						$isFinished = true;
					}
				}

				$asistReg = new AsistenciaLaboral();
				$asistReg->set($asis_data);

				if (strpos($asistReg->mensaje, 'Error') !== false) {
					throw new Exception("Registro inconsistente en la lectura del reloj biom�trico ($asistReg->mensaje)");
				}
				// 				echo "$asistReg : Entrada laboral<br/>";
			}
		}

		//Insert all the remaining assistance registers out of laboral schedule
		while ($regIdx < count($registro)) {
			$regIdx = self::setNoLaboralReg($codEmp, $registro, $regIdx, strtotime('+1 day'), $turno['tol_sal']);
		}
	}

	private function setNoLaboralReg($codEmp, $registro, $regIdx, $turnoHorario, $turnoTol) {
		$asis_data = array();
		$asis_data['codEmp'] = $codEmp;
		$asis_data['fecha'] = $registro[$regIdx]->getDate();
		$asis_data['turno'] = 0;
		$asis_data['hor_ent'] = $registro[$regIdx]->getTime();
		$asis_data['observado'] = true;
		$regIdx++;

		if (isset($registro[$regIdx]) && TimeUtils::diffDateTime($turnoHorario, $registro[$regIdx]->getTime()) > $turnoTol) {
			$asis_data['hor_sal'] = $registro[$regIdx]->getTime();
			$asis_data['observado'] = false;
			$regIdx++;
		} else {
			$asis_data['hor_sal'] = null;
		}

		$asistReg = new AsistenciaNoLaboral();
		$asistReg->set($asis_data);

		if (strpos($asistReg->mensaje, 'Error') !== false) {
			throw new Exception("Registro inconsistente en la lectura del reloj biom�trico ($asistReg->mensaje)");
		}

		// 		echo "$asistReg : Entrada no laboral<br/>";
		return $regIdx;
	}

	private function setSalInterReg($codEmp, $registro, $regIdx, $turno) {
		//Logica para marcado de salida intermedia
		// 						echo 'Logica para marcado de salida intermedia: ';
		$asis_data = array();
		$asis_data['codEmp'] = $codEmp;
		$asis_data['fecha'] = $registro[$regIdx]->getDate();
		$asis_data['turno'] = $turno["id_turno"];
		$asis_data['hor_sal'] = $registro[$regIdx]->getTime();
		$asis_data['observado'] = true;
		$regIdx++;

		if (isset($registro[$regIdx]) && TimeUtils::diffDateTime($turno["hora_sal"], $registro[$regIdx]->getTime()) > $turno["tol_sal"]) {
			$asis_data['hor_ent'] = $registro[$regIdx]->getTime();
			$asis_data['observado'] = false;
			$regIdx++;
		} else {
			$asis_data['hor_ent'] = null;
		}

		$asistReg = new SalidasIntermedias();
		$asistReg->set($asis_data);

		if (strpos($asistReg->mensaje, 'Error') !== false) {
			throw new Exception("Registro inconsistente en la lectura del reloj biom�trico ($asistReg->mensaje)");
		}

		// 		echo "$asistReg : Salida intermedia<br/>";
		return $regIdx;
	}
}

?>