<?php

# Importar modelo de abstraccion de base de datos
require_once('../core/db_abstract_model.php');
require_once('../core/db_abstract_asis.php');
require_once('../core/time_utils.php');
require_once('../asis_lab/model.php');

class SalidasIntermedias extends Asistencia {
	# Traer datos de un registro de asistencia
	public function get($codEmp = -1, $fecha = '', $turno = -1, $hor_sal = '') {
		if($codEmp != -1 && $fecha != '' && $turno != -1 && $hor_sal != '') {
			// 			$sqlFormat = "codEmp='%s' AND fecha = STR_TO_DATE('%s', '%%m/%%d/%%Y') AND turno='%s' AND hor_sal='%s'";
			$sqlFormat = "codEmp='%s' AND fecha = '%s' AND turno='%s' AND hor_sal='%s'";
			$sqlWhere = sprintf($sqlFormat, $codEmp, $fecha, $turno, $hor_sal);
			parent::get($sqlWhere, 'sal_inter');
		}
	}

	public function set($reg_data = array()) {
		foreach ($reg_data as $field=>$value) {
			$$field = $value;
		}

		if(isset($codEmp) && isset($fecha) && isset($turno) && isset($hor_sal)) {
			$regLikeDB = new SalidasIntermedias();
			$regLikeDB->get($codEmp, $fecha, $turno, $hor_sal);

			if(!$regLikeDB->codEmp) {
				// 				VALUES ('%s', STR_TO_DATE('%s', '%%m/%%d/%%Y'), '%s', '%s', '%s', '%s')";
				$sqlFormat = "INSERT INTO sal_inter (codEmp, fecha, turno, hor_ent, hor_sal, observado)
						VALUES ('%s', '%s', '%s', '%s', '%s', '%s')";
				$this->query = sprintf($sqlFormat, $codEmp, $fecha, $turno, $hor_ent, $hor_sal, $observado);
				$this->execute_single_query();
				$this->mensaje = 'Registro agregado exitosamente';
			} else {
				$this->mensaje = 'Error: El registro ya existe';
			}
		} else {
			$this->mensaje = 'Error: El registro no ha sido agregado';
		}
	}

	public function edit($reg_data=array()) {
		foreach ($reg_data as $field=>$value) {
			$$field = $value;
		}

		$regLab = new AsistenciaLaboral();
		$regLab->get($codEmp, $fecha, $turno);

		if(!$regLab->observado && !$observado &&
		TimeUtils::exists($hor_sal, $regLab->hor_ent, $regLab->hor_sal, false) &&
		TimeUtils::exists($hor_ent, $regLab->hor_ent, $regLab->hor_sal, false) &&
		$this->isValidDBData($reg_data)) {
			$tableName = 'sal_inter';
			parent::edit($reg_data, $tableName);
		} else {
			$this->mensaje = 'Error: Los valores modificados del registro no son v�lidos';
		}
	}

	# Eliminar un registro de asistencia
	public function delete($codEmp = -1, $fecha = '', $turno = -1, $hor_sal = '') {
		if($codEmp != -1 && $fecha != '' && $turno != -1 && $hor_sal != '') {
			// 			$sqlFormat = "codEmp='%s' AND fecha = STR_TO_DATE('%s', '%%m/%%d/%%Y') AND turno='%s' AND hor_sal='%s'";
			$sqlFormat = "codEmp='%s' AND fecha = '%s' AND turno='%s' AND hor_sal='%s'";
			$sqlWhere = sprintf($sqlFormat, $codEmp, $fecha, $turno, $hor_sal);
			parent::delete($sqlWhere, 'sal_inter');
		}
	}

	private function isValidDBData($reg_data){
		$isValid = true;
		foreach ($reg_data as $field=>$value) {
			$$field = $value;
		}

		$regSalInter = new SalidasIntermedias();
		// 		$queryFormat = "SELECT * FROM sal_inter WHERE codEmp='%s' AND fecha=STR_TO_DATE('%s', '%%m/%%d/%%Y') AND turno='%s' AND hor_sal!='%s'";
		$queryFormat = "SELECT * FROM sal_inter WHERE codEmp='%s' AND fecha='%s' AND turno='%s' AND hor_sal!='%s'";
		$regSalInter->query = sprintf($queryFormat, $codEmp, $fecha, $turno, $hor_sal);
		$regSalInter->get_results_from_query();
		$idx = 0;

		while ($isValid && $idx < count($regSalInter->rows)) {
			$row = $regSalInter->rows[$idx];
			$isValid = ($hor_sal < $row['hor_ent'] && $hor_ent < $row['hor_ent']) ||
			($hor_sal > $row['hor_sal'] && $hor_ent > $row['hor_sal']);
			$idx++;
		}

		return $isValid;
	}

	public function getAll() {
		try {
			$this->query = "SELECT * FROM sal_inter";
			$this->get_results_from_query();
			$this->mensaje = count($this->rows) > 0 ? 'Registros recuperados con �xito' : 'No existen registros';
			return $this->rows;
		} catch (Exception $e) {
			$this->mensaje = 'Error: ' . $e->getMessage();
		}
	}
}


?>