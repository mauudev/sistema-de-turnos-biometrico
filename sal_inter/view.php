<?php
require_once('../turnos/model.php');

$diccionario = array(
		'title'=>'Salidas intermedias',
		'subtitle'=>array(
				VIEW_ADD=>'Creaci�n de un nuevo registro',
				VIEW_SET=>'Creaci�n de un nuevo registro',
				VIEW_GET=>'Registro de asistencia',
				VIEW_DELETE=>'Eliminaci�n de registros',
				VIEW_EDIT=>'Modificaci�n de registros',
				VIEW_REPORT=>'Reporte de registros'
		),
		'page_info'=>array(
				VIEW_ADD=>'Esta p�gina permite a�adir un registro',
				VIEW_SET=>'Esta p�gina permite a�adir un registro',
				VIEW_GET=>'Esta p�gina permite ver/editar un registro',
				VIEW_DELETE=>'Esta p�gina permite eliminar un registro',
				VIEW_EDIT=>'Esta p�gina permite modificar un registro',
				VIEW_REPORT=>'Esta p�gina permite visualizar las salidas intermedias existentes'
		),
		'content_title'=>array(
				VIEW_ADD=>'Formulario de adici�n de un registro',
				VIEW_SET=>'Formulario de adici�n de un registro',
				VIEW_GET=>'Formulario de edici�n de un registro',
				VIEW_EDIT=>'Formulario de edici�n de un registro',
				VIEW_DELETE=>'Formulario de eliminaci�n un registro',
				VIEW_REPORT=>'Salidas intermedias'
		),
		'links_menu'=>array(
				'VIEW_ADD'=>'/webs/sis2/'.MODULO.'/'.VIEW_ADD.'/',
				'VIEW_SET'=>'/webs/sis2/'.MODULO.'/'.VIEW_SET.'/',
				'VIEW_GET'=>'/webs/sis2/'.MODULO.'/'.VIEW_GET.'/',
				'VIEW_EDIT'=>'/webs/sis2/'.MODULO.'/'.VIEW_EDIT.'/',
				'VIEW_DELETE'=>'/webs/sis2/'.MODULO.'/'.VIEW_DELETE.'/',
				'VIEW_REPORT'=>'/webs/sis2/'.MODULO.'/'.VIEW_REPORT.'/'
		),
		'form_actions'=>array(
				'ADD'=>'/webs/sis2/'.MODULO.'/'.ADD.'/',
				'SET'=>'/webs/sis2/'.MODULO.'/'.SET.'/',
				'GET'=>'/webs/sis2/'.MODULO.'/'.GET.'/',
				'DELETE'=>'/webs/sis2/'.MODULO.'/'.DELETE.'/',
				'EDIT'=>'/webs/sis2/'.MODULO.'/'.EDIT.'/',
				'REPORT'=>'/webs/sis2/'.MODULO.'/'.REPORT.'/'
		)
);

function get_template($form='site_template') {
	$file = '../site_media/html/'.$form.'.html';
	$template = file_get_contents($file);
	return $template;
}

function render_dinamic_data($html, $data) {
	foreach ($data as $clave=>$valor) {
		$html = str_replace('{'.$clave.'}', $valor, $html);
	}
	return $html;
}

function render_select_turnos($turnoId = 0, $readOnly = false) {
	$optStatus = $readOnly ? "disabled" : "";
	$r = "<select style='opacity: 0; width: 170px;' name='turno' id='turno' class='uniform' $optStatus>";

	$turnosDB = new Turno();
	$sqlWhere = $turnoId == 0 ? "WHERE habil = true" : "WHERE habil=true OR id_turno=$turnoId";
	$turnos = $turnosDB->getAll($sqlWhere);

	foreach ($turnos as $turno) {
		$r .= sprintf("<option value='%d'>%s</option>", $turno['id_turno'], $turno['descrip']);
	}

	$r .= "</select>";

	$turnoId = $turnoId != 0 ? $turnoId : $turnos[0]['id_turno'];
	$r .= sprintf("<script>document.getElementById('turno').value = %s;</script>",$turnoId);

	return $r;
}

function render_data_table($rows) {
	$html =
	"<table border='0' class='display data-table' id='example'>
			<thead>
			<tr>
			<th>CodEmp</th>
			<th>Fecha</th>
			<th>Turno</th>
			<th>Salida</th>
			<th>Entrada</th>
			<th>Estado</th>
			<th></th>
			</tr>
			</thead>
			<tbody>";

	foreach ($rows as $row) {
		$html .= "<tr class='gradeA'>";
		$html .= sprintf("<td class='center'>%s</td>", $row['codEmp']);
		$html .= sprintf("<td class='center'>%s</td>", $row['fecha']);

		$descrip = "No laboral";
		if ($row['turno'] != 0) {
			$turno = new Turno();
			$turno->get($row['turno']);
			$descrip = $turno->descrip;
		}
		$html .= sprintf("<td class='center'>%s</td>",  $descrip);

		$html .= sprintf("<td class='center'>%s</td>", $row['hor_sal']);
		$html .= sprintf("<td class='center'>%s</td>", $row['hor_ent']);
		$html .= sprintf("<td class='center'>%s</td>", $row['observado'] ? '<span class="observation-wrong">con errores</span>' : '<span class="observation-good">sin errores</span>');
		$html .= sprintf("
				<td>
				<a href='{GET}?codEmp=%1\$s&fecha=%2\$s&turno=%3\$s&hor_sal=%4\$s'>editar</a>&nbsp/
				<a href='{DELETE}?codEmp=%1\$s&fecha=%2\$s&turno=%3\$s&hor_sal=%4\$s'>eliminar</a>
				</td>",
				$row['codEmp'],
				$row['fecha'],
				$row['turno'],
				$row['hor_sal']);
		$html .= "</tr>";
	}

	$html .= "</tbody></table>";

	return $html;
}

function retornar_vista($vista, $data=array(), $msg = '') {
	global $diccionario;

	$html = get_template();
	$html = str_replace('{page_content}', get_template(MODULO.'_'.$vista), $html);

	if ($vista == VIEW_REPORT) {
		$html = str_replace('{data_table}', render_data_table($data), $html);
		$html = str_replace('{table_title}', $diccionario['content_title'][$vista],$html);
	} else {
		$html = str_replace('{form_title}', $diccionario['content_title'][$vista],$html);
		$html = render_dinamic_data($html, $data);

		if (isset($data['turno'])) {
			$html = str_replace('{turnos}', render_select_turnos($data['turno'], true), $html);
		} else {
			$html = str_replace('{turnos}', render_select_turnos(), $html);
		}
	}

	$html = str_replace('{title}', $diccionario['title'], $html);
	$html = str_replace('{subtitle}', $diccionario['subtitle'][$vista],$html);
	$html = str_replace('{page_info}', $diccionario['page_info'][$vista],$html);

	$html = render_dinamic_data($html, $diccionario['form_actions']);
	$html = render_dinamic_data($html, $diccionario['links_menu']);

	if ($msg == '') {
		$html = str_replace('{mensaje}', "<div id='box-msg' class='albox errorbox' style='visibility: hidden'></div>", $html);
	} else {
		if (strpos($msg, 'Error') === false ) {
			$msg = sprintf("<div id='box-msg' class='albox succesbox'>%s</div>", $msg);
		} else {
			$msg = sprintf("<div id='box-msg' class='albox errorbox'>%s</div>", $msg);
		}
		$html = str_replace('{mensaje}', $msg, $html);
	}

	print $html;
}

?>