<?php

$diccionario = array(
		'title'=>'Areas',
		'subtitle'=>array(
				VIEW_ADD=>'Creaci�n de una nueva �rea',
				VIEW_SET=>'Creaci�n de una nueva �rea',
				VIEW_GET=>'Registro de �rea',
				VIEW_DELETE=>'Eliminaci�n de �rea',
				VIEW_EDIT=>'Modificaci�n de �rea',
				VIEW_REPORT=>'Reporte de �reas'
		),
		'page_info'=>array(
				VIEW_ADD=>'Esta p�gina permite a�adir una �rea',
				VIEW_SET=>'Esta p�gina permite a�adir una �rea',
				VIEW_GET=>'Esta p�gina permite ver/editar una �rea',
				VIEW_DELETE=>'Esta p�gina permite eliminar una �rea',
				VIEW_EDIT=>'Esta p�gina permite modificar una �rea',
				VIEW_REPORT=>'Esta p�gina permite visualizar las �reas existentes'
		),
		'content_title'=>array(
				VIEW_ADD=>'Formulario de adici�n de una �rea',
				VIEW_SET=>'Formulario de adici�n de una �rea',
				VIEW_GET=>'Formulario de edici�n de una �rea',
				VIEW_EDIT=>'Formulario de edici�n de una �rea',
				VIEW_DELETE=>'Formulario de eliminaci�n una �rea',
				VIEW_REPORT=>'Areas'
		),
		'links_menu'=>array(
				'VIEW_ADD'=>'/webs/sis2/'.MODULO.'/'.VIEW_ADD.'/',
				'VIEW_SET'=>'/webs/sis2/'.MODULO.'/'.VIEW_SET.'/',
				'VIEW_GET'=>'/webs/sis2/'.MODULO.'/'.VIEW_GET.'/',
				'VIEW_EDIT'=>'/webs/sis2/'.MODULO.'/'.VIEW_EDIT.'/',
				'VIEW_DELETE'=>'/webs/sis2/'.MODULO.'/'.VIEW_DELETE.'/',
				'VIEW_REPORT'=>'/webs/sis2/'.MODULO.'/'.VIEW_REPORT.'/'
		),
		'form_actions'=>array(
				'ADD'=>'/webs/sis2/'.MODULO.'/'.ADD.'/',
				'SET'=>'/webs/sis2/'.MODULO.'/'.SET.'/',
				'GET'=>'/webs/sis2/'.MODULO.'/'.GET.'/',
				'DELETE'=>'/webs/sis2/'.MODULO.'/'.DELETE.'/',
				'EDIT'=>'/webs/sis2/'.MODULO.'/'.EDIT.'/',
				'REPORT'=>'/webs/sis2/'.MODULO.'/'.REPORT.'/'
		)
);

function get_template($form='site_template') {
	$file = '../site_media/html/'.$form.'.html';
	$template = file_get_contents($file);
	return $template;
}

function render_dinamic_data($html, $data) {
	foreach ($data as $clave=>$valor) {
		$html = str_replace('{'.$clave.'}', $valor, $html);
	}
	return $html;
}

function render_data_table($rows) {
	$html =
	"<table border='0' class='display data-table' id='example'>
			<thead>
			<tr>
			<th>Sigla</th>
			<th>Nombre</th>
			<th>Opciones</th>
			</tr>
			</thead>
			<tbody>";

	$area = new Area();

	foreach ($rows as $row) {
		$html .= "<tr class='gradeA'>";
		// 		$html .= sprintf("<td class='center'>%s</td>", $row['id_area']);
		$html .= sprintf("<td class='center'>%s</td>", $row['sigla_area']);
		$html .= sprintf("<td class='center'>%s</td>", $row['nombre_area']);

		if ($row['estado'] == 1) {
			$html .= sprintf("
					<td class='center'><a href='{GET}?id_area=%1\$d'>editar</a>&nbsp/
					<a href='{DELETE}?id_area=%1\$d&estado=0'>desactivar</a>
					</td>",
					$row['id_area']);
		} else {
			$html .= sprintf("
					<td class='center'><a href='{GET}?id_area=%1\$d'>editar</a>&nbsp/
					<a href='{DELETE}?id_area=%1\$d&estado=1'>activar</a>
					</td>",
					$row['id_area']);
		}

		$html .= "</tr>";
	}

	$html .= "</tbody></table>";

	return $html;
}

function retornar_vista($vista, $data=array(), $msg = '', $active = true) {
	global $diccionario;

	$html = get_template();
	$html = str_replace('{page_content}', get_template(MODULO.'_'.$vista), $html);

	if ($vista == VIEW_REPORT) {
		$html = str_replace('{data_table}', render_data_table($data), $html);
		$tableTitle = sprintf("%s %s", $diccionario['content_title'][$vista], $active ? 'activas' : 'inactivas');
		$html = str_replace('{table_title}', $tableTitle, $html);
	} else {
		$html = str_replace('{form_title}', $diccionario['content_title'][$vista], $html);
		$html = render_dinamic_data($html, $data);
	}

	$html = str_replace('{title}', $diccionario['title'], $html);
	$html = str_replace('{subtitle}', $diccionario['subtitle'][$vista],$html);
	$html = str_replace('{page_info}', $diccionario['page_info'][$vista],$html);

	$html = render_dinamic_data($html, $diccionario['form_actions']);
	$html = render_dinamic_data($html, $diccionario['links_menu']);
	$html = str_replace('{estado}', $active, $html);

	if ($msg == '') {
		$html = str_replace('{mensaje}', "<div id='box-msg' class='albox errorbox' style='visibility: hidden'></div>", $html);
	} else {
		if (strpos($msg, 'Error') === false ) {
			$msg = sprintf("<div id='box-msg' class='albox succesbox'>%s</div>", $msg);
		} else {
			$msg = sprintf("<div id='box-msg' class='albox errorbox'>%s</div>", $msg);
		}
		$html = str_replace('{mensaje}', $msg, $html);
	}

	print $html;
}

?>