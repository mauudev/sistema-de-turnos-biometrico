<?php

require_once('../core/db_abstract_model.php');

class Area extends DBAbstractModel{

	public $nombre_area;
	public $sigla_area;
	public $estado;
	public $id_area;

	function __construct(){
		$this->db_name = 'ctrl_asis';
	}

	function __destruct() {
		unset($this);
	}
	# Traer datos de una area
	public function get($id_area = ''){
		if($id_area != '') {
			$this->query = "
			SELECT *
			FROM area
			WHERE id_area = '$id_area'
			";
			$this->get_results_from_query();
			if(count($this->rows) == 1) {
				foreach ($this->rows[0] as $propiedad=>$valor) {
					$this->$propiedad = $valor;
				}
				$this->mensaje = 'Area encontrada';
			} else {
				$this->mensaje = 'Error: Area no encontrada';
			}
		}
	}
	# Crear una nueva area
	public function set($data=array()) {
		foreach($data as $campo => $valor)
			$$campo = $valor;

		try {
			$existsCond = sprintf("WHERE nombre_area='%s'", $data['nombre_area']);
			$idemCargos = $this->getAll($existsCond);
			if (count($idemCargos) > 0)
				throw new Exception('El nombre del area ya existe en los registros');
			
			$this->query = "INSERT INTO area
			(nombre_area,sigla_area,estado)
			VALUES
			('$nombre_area','$sigla_area', $estado)";
			$this->execute_single_query();
			$this->mensaje = 'Area registrada correctamente!';
		} catch (Exception $e) {
			$this->mensaje = sprintf("Error: Area no registrada (%s)", $e->getMessage());
		}
	}
	# Modificar una area
	public function edit($data=array()){
		if(array_key_exists('id_area', $data)) {
			foreach($data as $field => $value)
				$$field = $value;
				
			try {
				$this->query = sprintf("
						UPDATE area SET
						nombre_area='%s',
						sigla_area='%s',
						estado='%s'
						WHERE id_area = %d",
						$nombre_area,
						$sigla_area,
						$estado,
						$id_area);
				$this->execute_single_query();
				$this->mensaje = 'Area modificada correctamente';
			} catch (Exception $e) {
				$this->mensaje = sprintf("Error: Area no actualizada (%s)", $e->getMessage());
			}
		}
	}
	# Eliminar una area
	public function delete($id_area='', $isActive=false){
		if($id_area != '') {
			try {
				$this->query = sprintf("
						UPDATE area
						SET estado = $isActive
						WHERE id_area = %d",
						$id_area);
				$this->execute_single_query();
				$this->mensaje = sprintf('Area %s correctamente', $isActive ? 'activada' : 'desactivada');
			} catch (Exception $e) {
				$this->mensaje = sprintf("Error: No se puede cambiar el estado del registro (%s)", $e->getMessage());
			}
		}
	}


	# lista todas la areas
	public function getAll($where = '') {
		try {
			$this->query = "SELECT * FROM area $where";
			$this->get_results_from_query();
			$this->mensaje = count($this->rows) > 0 ? 'Registros recuperados con �xito' : 'No existen registros';
			return $this->rows;
		} catch (Exception $e) {
			$this->mensaje = "Error: " . $e->getMessage();
		}

	}
}

?>