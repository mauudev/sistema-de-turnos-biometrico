<?php
require_once('constants.php');
require_once('model.php');
require_once('view.php');

function handler() {
	$event = VIEW_REPORT;
	$uri = $_SERVER['REQUEST_URI'];
	$peticiones = array(
			ADD,
			SET,
			GET,
			DELETE,
			EDIT,
			REPORT,
			VIEW_ADD,
			VIEW_SET,
			VIEW_GET,
			VIEW_DELETE,
			VIEW_REPORT,
			VIEW_EDIT);
	foreach ($peticiones as $peticion) {
		$uri_peticion = MODULO.'/'.$peticion.'/';
		if( strpos($uri, $uri_peticion) == true ) {
			$event = $peticion;
		}
	}

	$data = helper_data();
	$area = set_obj();
	$area->estado = $data['estado'];
	switch ($event) {
		case ADD:
			$data = array(
			'nombre_area'=>'',
			'sigla_area'=>'',
			'estado'=>$area->estado
			);
			retornar_vista(VIEW_ADD, $data, '', $area->estado);
			break;
		case SET:
			$area->set($data);
			$msg = $area->mensaje;
			if (strpos($msg, 'Error') === false) {
				$sqlWhere = sprintf("WHERE estado = '%s'", $data['estado']);
				$data = $area->getAll($sqlWhere);
				retornar_vista(VIEW_REPORT, $data, $msg, $area->estado);
			} else {
				retornar_vista(VIEW_ADD, $data, $msg);
			}
			break;
		case GET:
			$area->get($data['id_area']);
			$data = array(
					'nombre_area'=>$area->nombre_area,
					'sigla_area'=>$area->sigla_area,
					'id_area'=>$area->id_area
			);
			retornar_vista(VIEW_GET, $data, $area->mensaje, $area->estado);
			break;
		case DELETE:
			$area->delete($data['id_area'], $data['estado']);
			$msg = $area->mensaje;
			$sqlWhere = sprintf("WHERE estado = '%s'", $data['estado']);
			$data = $area->getAll($sqlWhere);
			retornar_vista(VIEW_REPORT, $data, $msg, $area->estado);
			break;
		case EDIT:
			$area->edit($data);
			$sqlWhere = sprintf("WHERE estado = '%s'", $data['estado']);
			$msg = $area->mensaje;
			if (strpos($msg, 'Error') === false) {
				$data = $area->getAll($sqlWhere);
				retornar_vista(VIEW_REPORT, $data, $msg, $area->estado);
			} else {
				retornar_vista(VIEW_EDIT, array(), $msg);
			}
			break;
		case REPORT:
			$sqlWhere = sprintf("WHERE estado = '%s'", $data['estado']);
			$data = $area->getAll($sqlWhere);
			retornar_vista(VIEW_REPORT, $data, $area->mensaje, $area->estado);
			break;
		default:
			retornar_vista($event);
	}
}

function set_obj() {
	$obj = new Area();
	return $obj;
}

function helper_data() {
	$data = array();
	$data['estado'] = true;

	if($_POST) {
		if(array_key_exists('id_area', $_POST))
			$data['id_area'] = htmlentities($_POST['id_area']);
		if(array_key_exists('nombre_area', $_POST))
			$data['nombre_area'] = htmlentities($_POST['nombre_area']);
		if(array_key_exists('sigla_area', $_POST))
			$data['sigla_area'] = htmlentities($_POST['sigla_area']);
		if(array_key_exists('estado', $_POST))
			$data['estado'] = htmlentities($_POST['estado']);
	} else if($_GET) {
		if(array_key_exists('id_area', $_GET))
			$data['id_area'] = htmlentities($_GET['id_area']);
		if(array_key_exists('estado', $_GET))
			$data['estado'] =  htmlentities($_GET['estado']);
	}

	return $data;
}

handler();

?>
