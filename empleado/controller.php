<?php
require_once('constants.php');
require_once('model.php');
require_once('view.php');

function handler() {
	$event = VIEW_REPORT_USER;
	$uri = $_SERVER['REQUEST_URI'];
	//$uri = str_replace ("?", "", $uri);
	//echo "<br>uri: $uri";
	$peticiones = array(ADD_USER, SET_USER, GET_USER, DELETE_USER, EDIT_USER,REPORT_USER, VIEW_ADD_USER,
			VIEW_SET_USER, VIEW_GET_USER, VIEW_DELETE_USER, VIEW_REPORT_USER,
			VIEW_EDIT_USER);
	//echo "<br>SET_USER: ". SET_USER;
	foreach ($peticiones as $peticion) {
		//echo "<br>peticion: $peticion";
		$uri_peticion = MODULO.'/'.$peticion;
		//echo "<br>uri_peticion $uri_peticion";
		if( strpos($uri, $uri_peticion) == true ) {
			$event = $peticion;
		}
	}
	$empleado_data = helper_user_data();
	$empleado = set_obj();
	$empleado->estado = $empleado_data['estado'];
	// 	echo "<br>event: $event";

	switch ($event) {
		case ADD_USER:
			require_once('../cargos/model.php');
			require_once('../areas/model.php');
			$cargosDB = new Cargo();
			$cargos = $cargosDB->getAll('WHERE estado=1');
			$areasDB = new Area();
			$areas = $areasDB->getAll('WHERE estado=1');

			if (count($areas) > 0 && count($cargos) > 0) {
				$empleado_data = array('nombre'=>'', 'paterno'=>'','materno'=>'','direccion'=>'');
				retornar_vista(VIEW_ADD_USER, $empleado_data, '', $empleado_data['estado']);
			} else {
				$msg = "Error: no existen cargos o �reas activas disponibles para asignar";
				$empleado_data = $empleado->getAll($sqlWhere);
				retornar_vista(VIEW_REPORT_USER, $empleado_data, $msg, $empleado->estado);
			}			
			break;
		case SET_USER:
			$empleado->set($empleado_data);
			$sqlWhere = sprintf("WHERE estado = '%d'", $empleado_data['estado']);
			$msg = $empleado->mensaje;
			if (strpos($msg, 'Error') === false) {
				$empleado_data = $empleado->getAll($sqlWhere);
				retornar_vista(VIEW_REPORT_USER, $empleado_data, $msg, $empleado->estado);
			} else retornar_vista(VIEW_ADD_USER, $empleado_data, $msg);
			break;
		case GET_USER:
			$empleado->get($empleado_data['id_empleado']);
			$data = array(
					'id_empleado'=>$empleado->id_empleado,
					'id_cargo'=>$empleado->id_cargo,
					'id_area'=>$empleado->id_area,
					'nombre'=>$empleado->nombre,
					'paterno'=>$empleado->paterno,
					'materno'=>$empleado->materno,
					'direccion'=>$empleado->direccion,
					'estado'=>$empleado->estado
			);
			retornar_vista(VIEW_GET_USER, $data, $empleado->mensaje, $empleado->estado);
			break;
		case DELETE_USER:
			$empleado->delete($empleado_data['id_empleado'], $empleado_data['estado']);
			$msg = $empleado->mensaje;			
			$estado = strpos($msg, 'Error') === false ? $empleado_data['estado'] : !$empleado_data['estado'];
			$sqlWhere = sprintf("WHERE estado = '%d'", $estado);
			$empleado_data = $empleado->getAll($sqlWhere);
			retornar_vista(VIEW_REPORT_USER, $empleado_data, $msg, $estado);
			break;
		case EDIT_USER:
// 			print_r($empleado_data);
			$empleado->edit($empleado_data);
			$sql = sprintf("WHERE estado = '%d'", $empleado_data['estado']);
			$mensaje = $empleado->mensaje;
			if (strpos($mensaje, 'Error') === false) {
				$data = $empleado->getAll($sql);
				retornar_vista(VIEW_REPORT_USER, $data, $mensaje, $empleado_data['estado']);
			} else {
				retornar_vista(VIEW_GET_USER, $empleado_data, $mensaje);
			}
			break;
		case REPORT_USER:
			$sql = sprintf("WHERE estado = '%d'", $empleado_data['estado']);
			$activos = $empleado->getAll($sql);
			retornar_vista(VIEW_REPORT_USER, $activos, $empleado->mensaje, $empleado->estado);
			break;
		default:
			retornar_vista($event);
	}
}

function set_obj() {
	$obj = new Empleado();
	return $obj;
}

function helper_user_data() {
	$empleado_data = array();
	$empleado_data['estado'] = true;
	if($_POST) {
		if(array_key_exists('id_empleado', $_POST))
			$empleado_data['id_empleado'] = htmlentities($_POST['id_empleado']);
		if(array_key_exists('nombre', $_POST))
			$empleado_data['nombre'] = htmlentities($_POST['nombre']);
		if(array_key_exists('paterno', $_POST))
			$empleado_data['paterno'] = htmlentities($_POST['paterno']);
		if(array_key_exists('materno', $_POST))
			$empleado_data['materno'] = htmlentities($_POST['materno']);
		if(array_key_exists('cargo', $_POST))
			$empleado_data['id_cargo'] = htmlentities($_POST['cargo']);
		if(array_key_exists('area', $_POST))
			$empleado_data['id_area'] = htmlentities($_POST['area']);
		if(array_key_exists('direccion', $_POST))
			$empleado_data['direccion'] = htmlentities($_POST['direccion']);
		if(array_key_exists('estado', $_POST))
			$empleado_data['estado'] = htmlentities($_POST['estado']);
	}
	if($_GET){
		if(array_key_exists('estado', $_GET))
			$empleado_data['estado'] = htmlentities($_GET['estado']);
		if(array_key_exists('id_empleado', $_GET))
			$empleado_data['id_empleado'] = htmlentities($_GET['id_empleado']);
	}
	return $empleado_data;
}
handler();
?>
