<?php
# Importar modelo de abstracción de base de datos
require_once('../core/db_abstract_model.php');
class Empleado extends DBAbstractModel {
	############################### PROPIEDADES ################################
	//protected $id_empleado;
	public $id_empleado, $id_cargo, $id_area, $nombre, $paterno, $materno, $direccion, $estado;
	################################# MÉTODOS ##################################
	# Traer datos de una empleado
	#public function get($nombre='', $paterno='', $materno='') {
	public function get($id = '', $isRawData = true) {
		if($id != '') {
			$this->rows = array();

			try {
				if ($isRawData) {
					$this->query = "SELECT * FROM empleado WHERE id_empleado = $id";
				} else {
					$this->query =
					"SELECT e.id_empleado, c.id_cargo, c.nombre_cargo, a.id_area, a.nombre_area,
					e.nombre, e.paterno, e.materno, e.direccion, e.estado
					FROM empleado e, cargo c, area a
					WHERE e.id_cargo = c.id_cargo AND
					e.id_area = a.id_area AND
					e.id_empleado=$id
					";
				}
				$this->get_results_from_query();
				$this->mensaje = 'Registro encontrado';
			} catch(Exception $e) {
				$this->mensaje = 'Error: Registro no encontrado';
			}

			if(count($this->rows) == 1) {
				foreach ($this->rows[0] as $propiedad=>$valor)
					$this->$propiedad = $valor;

				return $this->rows[0];
				$this->mensaje = 'Empleado encontrado';
			} else {
				$this->mensaje = 'Error: Empleado no encontrado';
			}
		} else {
			$this->mensaje = 'Error: Consulta incorrecta';
		}
	}

	# A�adir un nuevo empleado
	public function set($empleado_data=array()) {
		if(array_key_exists('id_empleado', $empleado_data)){
			if($this->isValidUpdate($empleado_data)){
				foreach ($empleado_data as $campo=>$valor)
					$this->$campo = $valor;
				try{
					$this->query = "
					INSERT INTO empleado(id_cargo, id_area, nombre, paterno, materno, direccion, estado)
					VALUES('$this->id_cargo', '$this->id_area', '$this->nombre', '$this->paterno', '$this->materno', '$this->direccion', '1')";
					$this->execute_single_query();
					$this->mensaje = 'Empleado agregado exitosamente';
				}catch(Exception $e){
				}
			} else
				$this->mensaje = 'Error: Conflicto de cargo �nico en �rea para el registro de empleado';
		} else
			$this->mensaje = "Error: no existen datos de entrada";
	}

	# Modificar un empleado
	public function edit($data=array()){
		if(array_key_exists('id_empleado',$data)) {
			foreach($data as $field => $value)
				$$field = $value;
			try{
				$this->query = "UPDATE empleado
				SET id_cargo = '$id_cargo',
				id_area = '$id_area',
				nombre = '$nombre',
				paterno = '$paterno',
				materno = '$materno',
				direccion = '$direccion'
				WHERE id_empleado = '$id_empleado'";
				$this->execute_single_query();
				$this->mensaje = 'Datos actualizados correctamente!';
			}catch(Exception $e){
				$this->mensaje = 'Error: Se encontraron errores en la conexi�n con la base de datos';
			}
		} else
			$this->mensaje = 'Falta un id de empleado';
	}

	# Eliminar un empleado
	public function delete($id_empleado='', $isActive = false){
		if($id_empleado != '') {
			try {
				$this->get($id_empleado);
				$isValidDel = $this->estado ? true : $this->isValidUpdate($this->rows[0]);

				if($isValidDel) {
					$this->query = sprintf("UPDATE empleado
							SET	   estado = $isActive
							WHERE id_empleado = %d",$id_empleado);
					$this->execute_single_query();
					$this->mensaje = sprintf('Empleado %s correctamente', $isActive ? 'activado' : 'desactivado');
				} else {
					$this->mensaje = 'Error: Conflicto de cargo �nico en �rea para el registro de empleado';
				}
			} catch (Exception $e) {
				$this->mensaje = "Error: No se puedo cambiar el estado del registro ($e->getMessage())";
			}
		}
	}

	public function getAll($where = '') {
		try {
			$this->rows = array();
			$this->query = "SELECT * FROM empleado $where";
			$this->get_results_from_query();
			$this->mensaje = count($this->rows) > 0 ? 'Registros recuperados con �xito' : 'No existen registros';
			return $this->rows;
		} catch (Exception $e) {
			$this->mensaje = "Error: $e->getMessage()";
		}
	}

	# Obtener todos los empleados
	public function listAll($isRawData = true) {
		if ($isRawData) {
			$this->query = "SELECT * FROM empleado WHERE estado = 1";
		} else {
			$this->query =
			"SELECT e.id_empleado, c.nombre_cargo, a.nombre_area,
					CONCAT(e.nombre, ' ', e.paterno, ' ', e.materno) as nombre_completo, e.direccion, e.estado
					FROM empleado e, cargo c, area a
					WHERE e.id_cargo = c.id_cargo AND
					e.id_area = a.id_area AND
					e.estado=1
					";
		}

		$this->get_results_from_query();

		if(count($this->rows) != 0) {
			$this->mensaje = 'Empleados encontrados';
			return $this->rows;
		} else {
			$this->mensaje = 'Empleados no encontrados';
		}
	}

	public function isValidUpdate($data) {
		$this->rows = array();
		$this->query =
		sprintf("SELECT DISTINCT e.id_empleado
				FROM empleado e, cargo c, area a
				WHERE e.id_cargo = c.id_cargo AND
				e.id_area = a.id_area AND
				a.id_area = '%s' AND c.id_cargo = '%s'
				AND c.tipo_asig_area = 0 AND e.estado = 1",
				$data['id_area'],
				$data['id_cargo']);

		$this->get_results_from_query();
// 		echo $this->query;
		return count($this->rows) < 1 ? true: false;
	}

	# Metodo constructor
	function __construct() {
		//$this->db_name = 'vh_mvc';
	}
	# Metodo destructor del objeto
	function __destruct() {
		unset($this);
	}
}
?>
