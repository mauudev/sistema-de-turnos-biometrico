<?php
$diccionario = array(
		'subtitle'=>array(
				VIEW_ADD_USER=>'Creacion de un nuevo empleado',
				VIEW_SET_USER=>'Creacion de un nuevo empleado',
				VIEW_GET_USER=>'Registro de empleado',
				VIEW_DELETE_USER=>'Eliminacion de empleados',
				VIEW_EDIT_USER=>'Modificacion de empleados',
				VIEW_REPORT_USER=>'Reporte de empleados'
		),
		'page_info'=>array(
				VIEW_ADD_USER=>'Esta pagina permite añadir un nuevo empleado',
				VIEW_SET_USER=>'Esta pagina permite añadir un nuevo empleado',
				VIEW_GET_USER=>'Esta pagina permite ver/editar los datos de un empleado',
				VIEW_DELETE=>'Esta pagina permite eliminar un usuario',
				VIEW_EDIT_USER=>'Esta pagina permite modificar datos de un empleado',
				VIEW_REPORT_USER=>'Recuperar datos de todos los empleados'
		),
		'content_title'=>array(
				VIEW_ADD_USER=>'Formulario de adicion de un empleado',
				VIEW_SET_USER=>'Formulario de adicion de un empleado',
				VIEW_GET_USER=>'Formulario de edici&oacute;n de un empleado',
				VIEW_EDIT_USER=>'Formulario de edici&oacute;n de un empleado',
				VIEW_DELETE_USER=>'Formulario de eliminaci&oacute;n un empleado',
				VIEW_REPORT_USER=>'Empleados'
		),
		'links_menu'=>array(
				'VIEW_ADD_USER'=>MODULO.'/'.VIEW_ADD_USER.'/',
				'VIEW_SET_USER'=>MODULO.'/'.VIEW_SET_USER.'/',
				'VIEW_GET_USER'=>MODULO.'/'.VIEW_GET_USER.'/',
				'VIEW_EDIT_USER'=>MODULO.'/'.VIEW_EDIT_USER.'/',
				'VIEW_DELETE_USER'=>MODULO.'/'.VIEW_DELETE_USER.'/',
				'VIEW_REPORT_USER'=>MODULO.'/'.VIEW_REPORT_USER.'/'
		),
		'form_actions'=>array(
				'ADD'=>PATH_1.MODULO.'/'.ADD_USER.'/',
				'SET'=>PATH_1.MODULO.'/'.SET_USER.'/',
				'GET'=>PATH_1.MODULO.'/'.GET_USER.'/',
				'DELETE'=>PATH_1.MODULO.'/'.DELETE_USER.'/',
				'EDIT'=>PATH_1.MODULO.'/'.EDIT_USER.'/',
				'REPORT'=>PATH_1.MODULO.'/'.REPORT_USER.'/'
		));


function get_template($form='site_template') {
	$file = '../site_media/html/'.$form.'.html';
	$template = file_get_contents($file);
	return $template;
}

function get_key_value($table, $dato){
	$value = "";
	switch($table){
		case 'area':
			require_once("../areas/model.php");
			$area = new Area();
			$array_data = $area->getAll();
			foreach($array_data as $row){
				if($row['id_area'] == $dato)
					$value = sprintf("%s",$row['nombre_area']);
			}
			break;
		case 'cargo':
			require_once("../cargos/model.php");
			$cargo = new Cargo();
			$array_data = $cargo->getAll();
			$res = '';
			foreach($array_data as $row){
				if($row['id_cargo'] == $dato)
					$value = sprintf("%s",$row['nombre_cargo']);
			}
			break;
	}
	return $value;
}
/*function render_select_area($html){
 require_once("../areas/model.php");
$c = new Area();
$arr = $c->getAll();

for($i=0; $i < count($arr);$i++){
//var_dump($arr[$i]["nombre_area"]);
$option_c = "\n  <option value=\"" . $arr[$i]["id_area"] . "\">" .
$arr[$i]["nombre_area"] . "</option>{OPTION_AREA}";
$html = str_replace("{OPTION_AREA}", $option_c, $html);
}
$html = str_replace("{OPTION_AREA}", "\n", $html);
return $html;
}*/

function render_select_cargo($html,$data = array()){
	require_once("../cargos/model.php");
	if(array_key_exists('id_cargo',$data)){
		$cargo = new Cargo();
		$id = $data['id_cargo'];
		$nombre = get_key_value('cargo',$data['id_cargo']);
		$where = "WHERE id_cargo != $id AND estado =1";
		$tmp = $cargo->getAll($where);
		$str = "<option value='$id'>$nombre</option>"."<br>";
		for($i = 0; $i<count($tmp); $i++)
			$str .= "<option value='".$tmp[$i]['id_cargo']."'>".$tmp[$i]['nombre_cargo']."</option>"."<br>";
		$html = str_replace("{OPTION_CARGO}",$str,$html);
	}else{
		$cargo = new Cargo();
		$tmp = $cargo->getAll("WHERE estado = 1");
		if(count($tmp) > 0){
		$str = "";
		for($i = 0; $i<count($tmp); $i++)
			$str .= "<option value='".$tmp[$i]['id_cargo']."'>".$tmp[$i]['nombre_cargo']."</option>"."<br>";
		$html = str_replace("{OPTION_CARGO}",$str,$html);
		}else{
			$msg = "<option value='-1'>No existen cargos</option>";
			$html = str_replace("{OPTION_CARGO}",$msg,$html);
			}
	}
	return $html;
}

function render_select_area($html,$data = array()){
	require_once("../areas/model.php");
	if(array_key_exists('id_area',$data)){
		$area = new Area();
		$id = $data['id_area'];
		$nombre = get_key_value('area',$data['id_area']);
		$where = "WHERE id_area != $id AND estado = 1";
		$tmp = $area->getAll($where);
		$str = "<option value='$id'>$nombre</option>"."<br>";
		for($i = 0; $i<count($tmp); $i++)
			$str .= "<option value='".$tmp[$i]['id_area']."'>".$tmp[$i]['nombre_area']."</option>"."<br>";
		$html = str_replace("{OPTION_AREA}",$str,$html);
	}else{
		$area = new Area();
		$tmp = $area->getAll("WHERE estado = 1");
		if(count($tmp) > 0){
		$str = "";
		for($i = 0; $i<count($tmp); $i++)
			$str .= "<option value='".$tmp[$i]['id_area']."'>".$tmp[$i]['nombre_area']."</option>"."<br>";
		$html = str_replace("{OPTION_AREA}",$str,$html);
		}else{
			$msg = "<option value='-1'>No existen areas</option>";
			$html = str_replace("{OPTION_AREA}",$msg,$html);
		}
	}
	return $html;
}



function render_dinamic_table($rows){
	require_once("../empleado/model.php");

	// 	$html ="<table border='0' class='display data-table' id='example'>
	// 			<thead>
	// 			<tr>
	// 			<th>ID Empleado</th>
	// 			<th>Cargo</th>
	// 			<th>Area</th>
	// 			<th>Nombre</th>
	// 			<th>Apellido paterno</th>
	// 			<th>Apellido materno</th>
	// 			<th>Direccion</th>
	// 			<th>Estado</th>
	// 			<th>Opciones</th>
	// 			</tr>
	// 			</thead>
	// 			<tbody>";

	$html ="<table border='0' class='display data-table' id='example'>
			<thead>
			<tr>
			<th>ID</th>
			<th>Cargo</th>
			<th>Area</th>
			<th>Nombre</th>
			<th>Apellido paterno</th>
			<th>Apellido materno</th>
			<th>Direccion</th>
			<th>Opciones</th>
			</tr>
			</thead>
			<tbody>";

	$emp = new Empleado();
	foreach ($rows as $row) {
		$html .= "<tr class='gradeA'>";
		$html .= sprintf("<td class='center'>%s</td>", $row['id_empleado']);
		$html .= sprintf("<td class='center'>%s</td>", get_key_value('cargo',$row['id_cargo']));
		$html .= sprintf("<td class='center'>%s</td>", get_key_value('area',$row['id_area']));
		$html .= sprintf("<td class='center'>%s</td>", $row['nombre']);
		$html .= sprintf("<td class='center'>%s</td>", $row['paterno']);
		$html .= sprintf("<td class='center'>%s</td>", $row['materno']);
		$html .= sprintf("<td class='center'>%s</td>", $row['direccion']);
		//opcional REVISAR

		if ($row['estado'] == true) {
			$html .= sprintf("
					<td><a href='{GET}?id_empleado=%1\$d'>editar</a>&nbsp/
					<a href='{DELETE}?id_empleado=%1\$d&estado=0'>desactivar</a>
					</td>",
					$row['id_empleado']);
		} else {
			$html .= sprintf("
					<td><a href='{GET}?id_empleado=%1\$d'>editar</a>&nbsp/
					<a href='{DELETE}?id_empleado=%1\$d&estado=1'>activar</a>
					</td>",
					$row['id_empleado']);
		}

		$html .= "</tr>";
	}
	$html .= "</tbody></table>";
	return $html;
}

function render_dinamic_data($html, $data) {
	foreach ($data as $clave=>$valor) {
		$html = str_replace('{'.$clave.'}', $valor, $html);
	}
	return $html;
}

function render_select_empleado($html){
	require_once("../empleado/model.php");
	$c = new Empleado();
	$arr = $c->getAll();

	for($i=0; $i < count($arr);$i++){
		$option_c = "\n  <option value=\"" . $arr[$i]["id_empleado"] . "\">" .
				$arr[$i]["nombre"] . " " . $arr[$i]["paterno"] . " " . $arr[$i]["materno"] .
				"</option>{OPTION_JEFE}";
		$html = str_replace("{OPTION_JEFE}", $option_c, $html);
	}
	$html = str_replace("{OPTION_JEFE}", "\n", $html);
	return $html;
}

function retornar_vista($vista, $data=array(), $message = '', $active = true) {
	global $diccionario;
	$html = get_template();

	//renderizar campos
	$html = str_replace('{title}', "Empleado",$html);//ok
	$html = str_replace('{subtitle}', $diccionario['subtitle'][$vista],$html);//ok
	$html = str_replace('{page_info}', $diccionario['page_info'][$vista],$html);
	$html = str_replace('{page_content}', get_template(MODULO.'_'.$vista), $html);//ok
	$html = str_replace('{form_title}',$diccionario['content_title'][$vista],$html);//ok
	$html = render_dinamic_data($html, $diccionario['links_menu']);//ok
	//renderizar datos
	if ($vista == VIEW_REPORT_USER) {
		$html = str_replace('{data_table}', render_dinamic_table($data), $html);//ok
		$tableTitle = sprintf("%s %s", $diccionario['content_title'][$vista], $active ? 'activos' : 'inactivos');//ok
		$html = str_replace('{table_title}', $tableTitle, $html);//ok
	}else {
		$html = str_replace('{form_title}', $diccionario['content_title'][$vista], $html);//ok
		$html = render_dinamic_data($html, $data);//ok
	}
	if($vista == VIEW_EDIT_USER){
		//nada que hacer :(
	}

	$html = render_dinamic_data($html, $diccionario['form_actions']);//ok
	$html = str_replace('{estado}', $active, $html);//ok
	//$html = render_dinamic_data($html, $data);

	// render {MENSAJE}
	if ($message == '') {
		$html = str_replace('{mensaje}', "<div id='box-msg' class='albox errorbox' style='visibility: hidden'></div>", $html);//ok
	} else {
		if (strpos($message, 'Error') === false ) {
			$message = sprintf("<div id='box-msg' class='albox succesbox'>%s</div>", $message);//ok
		} else {
			$message = sprintf("<div id='box-msg' class='albox errorbox'>%s</div>", $message);//ok
		}
		$html = str_replace('{mensaje}', $message, $html);//OK
	}

	$html = render_select_cargo($html,$data);
	$html = render_select_empleado($html);
	$html = render_select_area($html,$data);
	#OK
	print $html;
}
?>
