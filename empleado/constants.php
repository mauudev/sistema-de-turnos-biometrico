<?php
define("MODULO", "empleado");
define("PATH_1", "/webs/sis2/");
//const MODULO = 'areas/';
# controladores
define("ADD_USER", "add");
define("SET_USER", "set");
define("GET_USER", "get");
define("DELETE_USER", "delete");
define("EDIT_USER", "edit");
define("REPORT_USER", "list");

# vistas
define("VIEW_ADD_USER", "add");
define("VIEW_SET_USER", "list");
define("VIEW_GET_USER", "edit");
define("VIEW_DELETE_USER", "list");
define("VIEW_EDIT_USER", "list");
define("VIEW_REPORT_USER", "list");


?>
