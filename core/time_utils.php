<?php

class TimeUtils {
	public static function diffDateTime($end, $start) {
		if(!is_int($start)) $start = strtotime($start);
		if(!is_int($end)) $end = strtotime($end);

		// 	$date1=date_create("2013-03-15");
		// 	$date2=date_create("2013-12-12");
		// 	$diff=date_diff($date1,$date2);
		// 	echo $diff->format("Years:%Y,Months:%M,Days:%d,Hours:%H,Minutes:%i,Seconds:%s");

		$diff = $end - $start;

		return $diff/60;
	}

	public static function exists($time, $start, $end, $inclLimits = false, $tolIni = 0, $tolFin = 0) {
		$res = false;

		// Convert to timestamp
		$startTs = strtotime($start) + $tolIni * 60;
		$endTs = strtotime($end) + $tolFin * 60;
		$userTs = strtotime($time);

		// Check that user date is between start & end
		if ($inclLimits) {
			$res = $userTs == $startTs || $userTs == $endTs;
		}

		return $res || (($userTs > $startTs) && ($userTs < $endTs));
	}
}

?>