<?php

# Importar modelo de abstraccion de base de datos
require_once('db_abstract_model.php');

abstract class Asistencia extends DBAbstractModel {
	// PROPIEDADES
	public $codEmp;
	public $fecha;
	public $turno;
	public $hor_ent;
	public $hor_sal;
	public $observado;

	# Metodo constructor
	function __construct() {
		$this->db_name = 'ctrl_asis';
	}

	# Metodo destructor del objeto
	function __destruct() {
		unset($this);
	}

	# Traer datos de un registro de asistencia
	public function get($sqlWhere = '', $tableName = '') {
		if($sqlWhere != '' && $tableName != '') {
			$this->query = "SELECT * FROM $tableName WHERE $sqlWhere";
			$this->get_results_from_query();
		}
		if(count($this->rows) == 1) {
			foreach ($this->rows[0] as $propiedad=>$valor) {
				$this->$propiedad = $valor;
			}
			$this->mensaje = 'Registro encontrado';
		} else {
			$this->mensaje = 'Error: Registro no encontrado';
		}
	}

	# Modificar un registro de asistencia
	public function edit($reg_data=array(), $tableName = '') {
		foreach ($reg_data as $field=>$value) {
			$$field = $value;
		}

		// 		$sqlFormat = "UPDATE $tableName
		// 					  SET
		// 		              hor_ent='%s',
		// 		              hor_sal='%s',
		// 		              observado='%s'
		// 		              WHERE
		// 		              codEmp='%s' AND
		// 		              fecha=STR_TO_DATE('%s', '%%m/%%d/%%Y') AND
		// 		              turno='%s'";

		try {
			$sqlFormat = "UPDATE $tableName
			SET
			hor_ent='%s',
			hor_sal='%s',
			observado='%s'
			WHERE
			codEmp='%s' AND
			fecha='%s' AND
			turno='%s'";
			$this->query = sprintf($sqlFormat, $hor_ent, $hor_sal, $observado, $codEmp, $fecha, $turno);
			$this->execute_single_query();
			$this->mensaje = 'Registro modificado';
		} catch (Exception $e) {
			$this->mensaje = 'Error: ' . $e;
		}
	}

	# Eliminar un registro de asistencia
	public function delete($sqlWhere = '', $tableName = '') {
		try {
			$this->query = "DELETE FROM $tableName WHERE $sqlWhere";
			$this->execute_single_query();
			$this->mensaje = 'Registro eliminado';
		} catch (Exception $e) {
			$this->mensaje = 'Error: ' . $e;
		}
	}
}

?>