<?php

require_once('constants.php');
require_once('model.php');
require_once('view.php');

function handler() {
	$event = REPORT;
	$uri = $_SERVER['REQUEST_URI'];
	$peticiones = array(
			SET,
			DELETE,
			LIST_EDIT,
			REPORT);
	foreach ($peticiones as $peticion) {
		$uri_peticion = MODULO.'/'.$peticion.'/';
		if( strpos($uri, $uri_peticion) == true ) {
			$event = $peticion;
			break;
		}
	}

	$viewData = helper_data();
	$empTurnos = set_obj();

	switch ($event) {
		case SET:
			$empTurnos->set($viewData);
			$msg = $empTurnos->mensaje;

			require_once('../empleado/model.php');
			$empleados = new Empleado();
			$viewData['empleados'] = $empleados->listAll();

			if (isset($viewData['cod_emp'])) {
				$viewData['empleado'] = $empleados->get($viewData['cod_emp'], false);
					
				require_once('../turnos/model.php');
				$turnos = new Turno();
				$viewData['turnos'] = $turnos->getAll("WHERE habil=1");
					
				$viewData['emp_turnos'] = $empTurnos->getTurnos($viewData['cod_emp']);
			}

			retornar_vista(VIEW_SET, $viewData, $msg);
			break;
		case DELETE:
			$empTurnos->delete($viewData['cod_emp'], $viewData['id_turno']);
			$msg = $empTurnos->mensaje;

			require_once('../empleado/model.php');
			$empleados = new Empleado();
			$viewData['empleados'] = $empleados->listAll();

			if (isset($viewData['cod_emp'])) {
				$viewData['empleado'] = $empleados->get($viewData['cod_emp'], false);

				require_once('../turnos/model.php');
				$turnos = new Turno();
				$viewData['turnos'] = $turnos->getAll("WHERE habil=1");

				$turnosEmp = $empTurnos->getTurnos($viewData['cod_emp']);
				$viewData['emp_turnos'] = isset($turnosEmp) ? $turnosEmp : array();
			}

			retornar_vista(VIEW_DELETE, $viewData, $msg);
			break;
		case LIST_EDIT:
			require_once('../empleado/model.php');
			$empleados = new Empleado();
			$viewData['empleados'] = $empleados->listAll();
			$msg = $empleados->mensaje;

			if (isset($viewData['cod_emp'])) {
				$viewData['empleado'] = $empleados->get($viewData['cod_emp'], false);

				require_once('../turnos/model.php');
				$turnos = new Turno();
				$viewData['turnos'] = $turnos->getAll("WHERE habil=1");

				$turnosEmp = $empTurnos->getTurnos($viewData['cod_emp']);
				$viewData['emp_turnos'] = isset($turnosEmp) ? $turnosEmp : array();

				$msg = $empTurnos->mensaje;
			}
			retornar_vista(VIEW_LIST_EDIT, $viewData, $msg);
			break;
		case REPORT:
			$msg = '';				
			$empTurnosReg = $empTurnos->getAll();
			if (count($empTurnosReg) > 0) {
				$areas = $empTurnos->getAssignedAreas();
				foreach ($areas as $area) {
					$cargos = $empTurnos->getAssignedCargos($area['id_area']);
					$data = array();

					if (count($cargos) > 0) {
						foreach ($cargos as $cargo) {
							$rows = $empTurnos->getAll($area['id_area'], $cargo['id_cargo']);
							if (count($rows) > 0) {
								$data[$cargo['id_cargo']] = $rows;
							}
							$rows = array();
						}
						if (isset($data)) {
							$viewData[$area['nombre_area']] = $data;
							$msg = $empTurnos->mensaje;
						}
					}
				}
			} else {
				$viewData = array();								
			}	
			retornar_vista(VIEW_REPORT, $viewData, $msg);
			break;
		default:
			retornar_vista($event);
	}
}

function set_obj() {
	$obj = new EmpTurnos();
	return $obj;
}

function helper_data() {
	$data = array();

	if($_POST) {
		if(array_key_exists('cod_emp', $_POST))
			$data['cod_emp'] = htmlentities($_POST['cod_emp']);
		if(array_key_exists('id_turno', $_POST))
			$data['id_turno'] = htmlentities($_POST['id_turno']);
	} else if($_GET) {
		if(array_key_exists('cod_emp', $_GET))
			$data['cod_emp'] = htmlentities($_GET['cod_emp']);
		if(array_key_exists('id_turno', $_GET))
			$data['id_turno'] = htmlentities($_GET['id_turno']);
	}

	return $data;
}

handler();

?>
