<?php

require_once('../core/db_abstract_model.php');

class EmpTurnos extends DBAbstractModel{
	public $cod_emp;
	public $id_turno;

	function __construct(){
		$this->db_name = 'ctrl_asis';
	}

	function __destruct() {
		unset($this);
	}

	public function get($cod_emp = '', $id_turno = ''){
		if($cod_emp != '' && $id_turno != '') {
			$this->query = "SELECT * FROM emp_turno WHERE cod_emp = $codEmp AND id_turno = $id_turno";
			$this->get_results_from_query();
			if(count($this->rows) == 1) {
				foreach ($this->rows[0] as $propiedad=>$valor) {
					$this->$propiedad = $valor;
				}
				$this->mensaje = 'Registro encontrado';
				return $this->rows[0];
			} else {
				$this->mensaje = 'Error: Registro no encontrado';
			}
		}
	}

	public function set($data=array()) {
		foreach($data as $campo => $valor)
			$$campo = $valor;

		try {
			$this->query = "INSERT INTO emp_turno(cod_emp, id_turno)
			VALUES('$cod_emp', '$id_turno')";
			$this->execute_single_query();
			$this->mensaje = 'Datos registrados correctamente';
		} catch (Exception $e) {
			$this->mensaje = "Error: Datos no registrados ($e->getMessage())";
		}
	}

	public function edit($data=array()){
		// 		if(array_key_exists('id_turno', $data)) {
		// 			foreach($data as $field => $value)
		// 				$$field = $value;
			
		// 			try {
		// 				$this->query = sprintf("UPDATE emp_turno SET
		// 									cod_emp = '%s',
		// 									id_turno = '%s'",
		// 						$cod_emp,
		// 						$id_turno);
		// 				$this->execute_single_query();
		// 				$this->mensaje = 'Datos modificados correctamente';
		// 			} catch (Exception $e) {
		// 				$this->mensaje = "Error: Datos no actualizados ($e->getMessage())";
		// 			}
		// 		}
		$this->mensaje = "No es posible editar los turnos";
	}

	public function delete($cod_emp='', $id_turno=''){
		if($cod_emp != '' && $id_turno != '') {
			try {
				$this->query = "DELETE FROM emp_turno WHERE cod_emp=$cod_emp AND id_turno=$id_turno";
				$this->execute_single_query();
				$this->mensaje = 'Registro eliminado';
			} catch (Exception $e) {
				$this->mensaje = "Error: No es posible eliminar el registro ($e->getMessage())";
			}
		}
	}

	public function getTurnos($cod_emp) {
		try {
			$this->query = "SELECT et.cod_emp, t.id_turno, descrip, t.hora_ent, t.tol_ent, t.hora_sal, t.tol_sal
			FROM turno t, emp_turno et
			WHERE et.id_turno = t.id_turno AND et.cod_emp = $cod_emp";
			$this->get_results_from_query();
			if (count($this->rows) == 0)
				$this->mensaje = 'Turnos no asignados a empleado';
			else
				$this->mensaje = 'Registros recuperados con �xito';
			return $this->rows;
		} catch (Exception $e) {
			$this->mensaje = "Error: $e->getMessage()";
		}
	}

	// 	public function getAssignedEmpleados() {
	// 		try {
	// 			$this->rows = array();
	// 			$query = sprintf("
	// 					SELECT DISTINCT et.cod_emp, CONCAT(e.nombre, ' ', e.paterno, ' ', e.materno) as nombre_completo,
	// 					a.nombre_area, c.nombre_cargo
	// 					FROM empleado e, cargo c, area a, emp_turno et
	// 					WHERE e.id_empleado = et.cod_emp AND
	// 					e.id_cargo = c.id_cargo AND
	// 					e.id_area = a.id_area");
	// 			$this->get_results_from_query();
	// 			$this->mensaje = count($this->rows) > 0 ? 'Registros recuperados con �xito' : 'No existen registros';
	// 			return $this->rows;
	// 		} catch (Exception $e) {
	// 			$this->mensaje = "Error: $e->getMessage()";
	// 		}
	// 	}

	public function getAssignedAreas() {
		try {
			$this->rows = array();
			$this->query = "SELECT DISTINCT e.id_area, nombre_area FROM empleado e, area a WHERE e.id_area = a.id_area";
			$this->get_results_from_query();
			$this->mensaje = count($this->rows) > 0 ? 'Registros recuperados con �xito' : 'No existen registros';
			return $this->rows;
		} catch (Exception $e) {
			$this->mensaje = "Error: $e->getMessage()";
		}
	}

	public function getAssignedCargos($area = '') {
		try {
			$this->rows = array();
			$query = "SELECT DISTINCT id_cargo FROM empleado";
			if ($area != '')
				$query = sprintf("%s WHERE id_area = %s", $query, $area);

			
			$this->query = $query;
			$this->get_results_from_query();
			$this->mensaje = count($this->rows) > 0 ? 'Registros recuperados con �xito' : 'No existen registros';
			return $this->rows;
		} catch (Exception $e) {
			$this->mensaje = "Error: $e->getMessage()";
		}
	}

	public function getAll($area='', $cargo='') {
		try {
			$this->rows = array();
			$query = sprintf("
					SELECT et.cod_emp, CONCAT(e.nombre, ' ', e.paterno, ' ', e.materno) as nombre_completo,
					t.id_turno, t.descrip, a.nombre_area, c.nombre_cargo, t.hora_ent, t.hora_sal
					FROM empleado e, cargo c, area a, turno t, emp_turno et
					WHERE e.id_empleado = et.cod_emp AND
					t.id_turno = et.id_turno AND
					e.id_cargo = c.id_cargo AND
					e.id_area = a.id_area");

			if ($area != '') {
				$query = sprintf("%s AND e.id_area = %s", $query, $area);
			}

			if ($cargo != '') {
				$query = sprintf("%s AND e.id_cargo = %s", $query, $cargo);
			}
				
			
			$this->query = $query;
			$this->get_results_from_query();
			$this->mensaje = count($this->rows) > 0 ? 'Registros recuperados con �xito' : 'No existen registros';
			return $this->rows;
		} catch (Exception $e) {
			$this->mensaje = "Error: $e->getMessage()";
		}
	}

	public function hasTurnos($cod_emp) {
		try {
			$this->rows = array();
			$this->query = "SELECT * FROM emp_turno WHERE cod_emp = $cod_emp";
			$this->get_results_from_query();

			$this->mensaje = count($this->rows) > 0 ? 'Registros recuperados con �xito' : 'No existen registros';
			return count($this->rows) > 0;
		} catch (Exception $e) {
			$this->mensaje = "Error: $e->getMessage()";
		}
	}
}

?>