<?php

define("MODULO", "emp_turnos");

# controladores
define("ADD", "add");
define("SET", "set");
define("GET", "get");
define("DELETE", "delete");
define("EDIT", "edit");
define("LIST_EDIT", "list");
define("REPORT", "report");

# vistas
define("VIEW_ADD", "add");
define("VIEW_SET", "list");
define("VIEW_GET", "edit");
define("VIEW_DELETE", "list");
define("VIEW_EDIT", "list");
define("VIEW_LIST_EDIT", "list");
define("VIEW_REPORT", "report");

?>