<?php

$diccionario = array(
		'title'=>'Turnos',
		'subtitle'=>array(
				VIEW_ADD=>'Asignaci�n de turnos',
				VIEW_SET=>'Asignaci�n de turnos',
				VIEW_GET=>'Registro de una asignaci�n de turno',
				VIEW_DELETE=>'Eliminaci�n de asignac�n',
				VIEW_EDIT=>'Modificaci�n de asignaci�n',
				VIEW_LIST_EDIT=>'Lista de asignaciones',
				VIEW_REPORT=>'Reporte de turnos por �reas'
		),
		'page_info'=>array(
				VIEW_SET=>'Esta p�gina permite a�adir una asignaci�n',
				VIEW_DELETE=>'Esta p�gina permite eliminar una asignaci�n',
				VIEW_LIST_EDIT=>'Esta p�gina permite gestionar los turnos asignados a un empleado',
				VIEW_REPORT=>'Esta p�gina muestra los turnos asignados a un empleado'
		),
		'content_title'=>array(
				VIEW_SET=>'Formulario de asignaci�n de un turno',
				VIEW_DELETE=>'Formulario de eliminaci�n de una asignaci�n',
				VIEW_LIST_EDIT=>'Asignaci�n de turnos',
				VIEW_REPORT=>'Turnos'
		),
		'links_menu'=>array(
				'VIEW_SET'=>'/webs/sis2/'.MODULO.'/'.VIEW_SET.'/',
				'VIEW_DELETE'=>'/webs/sis2/'.MODULO.'/'.VIEW_DELETE.'/',
				'VIEW_LIST_EDIT'=>'/webs/sis2/'.MODULO.'/'.VIEW_LIST_EDIT.'/',
				'VIEW_REPORT'=>'/webs/sis2/'.MODULO.'/'.VIEW_REPORT.'/'
		),
		'form_actions'=>array(
				'SET'=>'/webs/sis2/'.MODULO.'/'.SET.'/',
				'DELETE'=>'/webs/sis2/'.MODULO.'/'.DELETE.'/',
				'LIST_EDIT'=>'/webs/sis2/'.MODULO.'/'.LIST_EDIT.'/',
				'REPORT'=>'/webs/sis2/'.MODULO.'/'.REPORT.'/'
		)
);

function get_template($form='site_template') {
	$file = '../site_media/html/'.$form.'.html';
	$template = file_get_contents($file);
	return $template;
}

function render_dinamic_data($html, $data) {
	foreach ($data as $clave=>$valor) {
		$html = str_replace('{'.$clave.'}', $valor, $html);
	}
	return $html;
}

// function render_select_turnos($turno = 0, $readOnly = false) {
// 	$optStatus = $readOnly ? "disabled" : "";
// 	$r = "<select style='opacity: 0; width: 170px;' name='turno' id='turno' class='uniform' $optStatus>";

// 	$turnosDB = new Turno();
// 	$turnos = $turnosDB->getAll();
// 	for ($i = 1; $i < count($turnos); $i++) {
// 		$r .= sprintf("<option value='%s'>%s</option>", $turnos[$i]['id_turno'], $turnos[$i]['descrip']);
// 	}

// 	$r .= "</select>";
// 	$r .= sprintf("<script>document.getElementById('turno').value = %s;</script>",$turno);

// 	return $r;
// }

function render_static_table($title, $rows) {
	$html =
	"<div class='simplebox grid740'><div class='titleh'><h3>$title</h3></div>
	<table class='tablesorter' id='example'>
	<thead>
	<tr>
	<th>Cod Emp</th>
	<th>Nombre Completo</th>
	<th>Turno</th>
	<th>Entrada</th>
	<th>Salida</th>
	</tr>
	</thead>
	<tbody>";

	foreach ($rows as $row) {
		$html .= "<tr class='gradeA'>";
		$html .= sprintf("<td class='center'>%s</td>", $row['cod_emp']);
		$html .= sprintf("<td class='center'>%s</td>", $row['nombre_completo']);
		$html .= sprintf("<td class='center'>%s</td>", $row['descrip']);
		$html .= sprintf("<td class='center'>%s</td>", $row['hora_ent']);
		$html .= sprintf("<td class='center'>%s</td>", $row['hora_sal']);

		$html .= "</tr>";
	}

	$html .= "</tbody></table></div>{data_table}";

	return $html;
}

function render_emp_turnos_table($rows) {
	$html =
	"<table id='turnos_table' class='tablesorter'>
			<thead>
			<tr>
			<th>Descripci�n</th>
			<th>Hora Entrada</th>
			<th>Hora Salida</th>
			<th>Opciones</th>
			</tr>
			</thead>
			<tbody>";

	foreach ($rows as $row) {
		$html .= "<tr>";
		$html .= sprintf("<td class='center'>%s</td>", $row['descrip']);
		$html .= sprintf("<td class='center'>%s</td>", $row['hora_ent']);
		$html .= sprintf("<td class='center'>%s</td>", $row['hora_sal']);
		$html .= sprintf("
				<td class='center'><a href='{DELETE}?cod_emp=%1\$d&id_turno=%2\$d'>eliminar</a>
				</td>",
				$row['cod_emp'],
				$row['id_turno']);

		$html .= "</tr>";
	}

	$html .= "</tbody></table>";

	return $html;
}

// function render_dynamic_table($rows) {
// 	$html ="<table border='0' class='display data-table' id='example'>
// 			<thead>
// 			<tr>
// 			<th>Cod Emp</th>
// 			<th>Nombre Completo</th>
// 			<th>Area</th>
// 			<th>Cargo</th>
// 			<th>Turno</th>
// 			<th>Entrada</th>
// 			<th>Salida</th>
// 			<th>Opciones</th>
// 			</tr>
// 			</thead>
// 			<tbody>";

// 	foreach ($rows as $row) {
// 		$html .= "<tr class='gradeA'>";
// 		$html .= sprintf("<td class='center'>%s</td>", $row['cod_emp']);
// 		$html .= sprintf("<td class='center'>%s</td>", $row['nombre_completo']);
// 		$html .= sprintf("<td class='center'>%s</td>", $row['nombre_area']);
// 		$html .= sprintf("<td class='center'>%s</td>", $row['nombre_cargo']);
// 		$html .= sprintf("<td class='center'>%s</td>", $row['descrip']);
// 		$html .= sprintf("<td class='center'>%s</td>", $row['hora_ent']);
// 		$html .= sprintf("<td class='center'>%s</td>", $row['hora_sal']);

// 		$html .= sprintf("
// 				<td><a href='{GET}?cod_emp=%1\$d&id_turno=%2\$d'>editar</a>&nbsp/
// 				<a href='{DELETE}?cod_emp=%1\$d&id_turno=%2\$d'>eliminar</a>
// 				</td>",
// 				$row['cod_emp'],
// 				$row['id_turno']);

// 		$html .= "</tr>";
// 	}

// 	$html .= "</tbody></table>";

// 	return $html;
// }

function render_combo($selectId, $rows, $rowVal, $rowDescrip = array()) {
	$html = "<select id='$selectId'><option value=''>Seleccionar una opci�n...</option>";
	if (isset($rows)) {
		foreach ($rows as $row) {
			$selDescrip = '';
			foreach ($rowDescrip as $descrip) {
				$selDescrip .= $row[$descrip] . ' ' ;
			}
			$html .= sprintf("<option value='%s'>%s</option>", $row[$rowVal], trim($selDescrip));
		}
	}
	$html .= "</select>";
	return $html;
}

function retornar_vista($vista, $data=array(), $msg = '') {
	global $diccionario;

	$html = get_template();
	$html = str_replace('{page_content}', get_template(MODULO.'_'.$vista), $html);

	switch ($vista) {
		case VIEW_REPORT:
			if (count($data) > 0) {
				foreach ($data as $area => $cargos) {
					foreach ($cargos as $cargo => $rows) {
						$title = "Cargo: " . $rows[0]['nombre_cargo'];
						$html = str_replace('{data_table}', render_static_table($title, $rows), $html);
						$html = str_replace('{section_title}', "Area: " . $area, $html);
					}
					$html = str_replace('{data_table}', '<hr/><br/><h1 class="h4-tag">{section_title}</h1><br/>{data_table}', $html);
				}
				$html = str_replace('<h1 class="h4-tag">{section_title}</h1><br/>{data_table}', '', $html);
			} else {
				$html = str_replace('{data_table}', '', $html);
				$html = str_replace('{section_title}', '', $html);
				$msg = "No existen registros para ser reportados";
			}
			break;
		CASE VIEW_LIST_EDIT:
			$html = str_replace('{empleados}',
			render_combo('emp_combo',
			$data['empleados'],
			'id_empleado',
			array('nombre', 'paterno', 'materno')), $html);

			if (isset($data['cod_emp'])) {
				$html = str_replace('{empleado_data_visibility}', 'visible', $html);
				$html = str_replace('{turnos_data_visibility}', 'visible', $html);
				$html = str_replace('{turnos}', render_combo('turnos_combo', $data['turnos'], 'id_turno', array('descrip')), $html);
				$html = render_dinamic_data($html, $data['empleado']);
				$html = str_replace('{emp_turnos_table}', render_emp_turnos_table($data['emp_turnos']), $html);
				$html = str_replace('{emp_combo_default}', $data['cod_emp'], $html);
			} else {
				$html = str_replace('{empleado_data_visibility}', 'hidden', $html);
				$html = str_replace('{turnos_data_visibility}', 'hidden', $html);
				$html = str_replace('{emp_combo_default}', '0', $html);
			}
			$html = str_replace('{turnos_combo_default}', isset($data['id_turno']) ? $data['id_turno'] : 0, $html);
			break;
		default:
			$html = str_replace('{form_title}', $diccionario['content_title'][$vista], $html);
			$html = render_dinamic_data($html, $data);
			break;
	}

	$html = str_replace('{title}', $diccionario['title'], $html);
	$html = str_replace('{subtitle}', $diccionario['subtitle'][$vista],$html);
	$html = str_replace('{page_info}', $diccionario['page_info'][$vista],$html);

	$html = render_dinamic_data($html, $diccionario['form_actions']);
	$html = render_dinamic_data($html, $diccionario['links_menu']);

	if ($msg == '') {
		$html = str_replace('{mensaje}', "<div id='box-msg' class='albox errorbox' style='visibility: hidden'></div>", $html);
	} else {
		if (strpos($msg, 'Error') === false ) {
			$msg = sprintf("<div id='box-msg' class='albox succesbox'>%s</div>", $msg);
		} else {
			$msg = sprintf("<div id='box-msg' class='albox errorbox'>%s</div>", $msg);
		}
		$html = str_replace('{mensaje}', $msg, $html);
	}

	print $html;
}

?>
