<?php
require_once('../reloj/model.php');

echo 'Testing<hr/>';

// ------------------------
// Test for reading the bio clock (Working)
// ------------------------

// $file = 'test1.txt';
// BioClockReader::getRegistros($file);

// ------------------------
// Test for get methods
// ------------------------

// //Salidas Intermedias (Working)
// $testGetSalInter = new SalidasIntermedias();
// $testGetSalInter->get(301315, '05/23/2013', 1, '08:10:12');
// @print_r($testGetSalInter);
// echo '<br/>';

// //Asistencia no laboral (Working)
// $testGetNoLaboral = new AsistenciaNoLaboral();
// $testGetNoLaboral->get(301310, '05/23/2013', 0, '07:15:45');
// @print_r($testGetNoLaboral);
// echo '<br/>';

// //Asistencia laboral (Working)
// $testGetLaboral = new AsistenciaLaboral();
// $testGetLaboral->get(301310, '05/23/2013', 1);
// @print_r($testGetLaboral);
// echo '<br/>';

// ------------------------
// Test for edit methods
// ------------------------

//Salidas Intermedias
// $testEditSalInter = new SalidasIntermedias();
// $testEditSalInter->get(301315, '05/23/2013', 1, '08:10:12');

// $testEditSalInter->hor_ent = '08:10:12';
// $regModSalInter = array(
// 		'codEmp'=>$testEditSalInter->codEmp,
// 		'fecha'=>date('m/d/y', strtotime($testEditSalInter->fecha)),
// 		'turno'=>$testEditSalInter->turno,
// 		'hor_ent'=>$testEditSalInter->hor_ent,
// 		'hor_sal'=>$testEditSalInter->hor_sal,
// 		'observado'=>false); //TODO: En la interface se debe cambiar este valor dado que se validan todos los campos obligatoriamente!!!
// print_r($regModSalInter);
// echo '<br/>';
// $testEditSalInter->edit($regModSalInter);
// @print_r($testEditSalInter);
// echo '<br/>';

// $testEditSalInter->hor_ent = '12:10:12';
// $regModSalInter = array(
// 		'codEmp'=>$testEditSalInter->codEmp,
// 		'fecha'=>date('m/d/y', strtotime($testEditSalInter->fecha)),
// 		'turno'=>$testEditSalInter->turno,
// 		'hor_ent'=>$testEditSalInter->hor_ent,
// 		'hor_sal'=>$testEditSalInter->hor_sal,
// 		'observado'=>false); //TODO: En la interface se debe cambiar este valor dado que se validan todos los campos obligatoriamente!!!
// print_r($regModSalInter);
// echo '<br/>';
// $testEditSalInter->edit($regModSalInter);
// @print_r($testEditSalInter);
// echo '<br/>';

// $testEditSalInter->hor_ent = '11:10:12';
// $regModSalInter = array(
// 		'codEmp'=>$testEditSalInter->codEmp,
// 		'fecha'=>date('m/d/y', strtotime($testEditSalInter->fecha)),
// 		'turno'=>$testEditSalInter->turno,
// 		'hor_ent'=>$testEditSalInter->hor_ent,
// 		'hor_sal'=>$testEditSalInter->hor_sal,
// 		'observado'=>false); //TODO: En la interface se debe cambiar este valor dado que se validan todos los campos obligatoriamente!!!
// print_r($regModSalInter);
// echo '<br/>';
// $testEditSalInter->edit($regModSalInter);
// @print_r($testEditSalInter);
// echo '<br/>';

// $testEditSalInter->hor_ent = '09:14:15';
// $regModSalInter = array(
// 		'codEmp'=>$testEditSalInter->codEmp,
// 		'fecha'=>date('m/d/y', strtotime($testEditSalInter->fecha)),
// 		'turno'=>$testEditSalInter->turno,
// 		'hor_ent'=>$testEditSalInter->hor_ent,
// 		'hor_sal'=>$testEditSalInter->hor_sal,
// 		'observado'=>false); //TODO: En la interface se debe cambiar este valor dado que se validan todos los campos obligatoriamente!!!
// print_r($regModSalInter);
// echo '<br/>';
// $testEditSalInter->edit($regModSalInter);
// @print_r($testEditSalInter);
// echo '<br/>';

// //Asistencia no laboral
// $testEditNoLaboral = new AsistenciaNoLaboral();
// $testEditNoLaboral->get(301310, '05/23/2013', 0, '07:15:45');
// @print_r($testEditNoLaboral);
// echo '<br/>';

// //Asistencia laboral
// $testEditLaboral = new AsistenciaLaboral();
// $testEditLaboral->get(301310, '05/23/2013', 1);
// @print_r($testEditLaboral);
// echo '<br/>';

// ------------------------
// Test for delete methods
// ------------------------



?>