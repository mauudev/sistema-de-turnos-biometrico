<?php
# Importar modelo de abstraccion de base de datos
require_once('../core/db_abstract_model.php');
require_once('../core/time_utils.php');
require_once('../reloj/db_abstract_asis.php');

abstract class Asistencia extends DBAbstractModel {
	// PROPIEDADES
	public $codEmp;
	public $fecha;
	public $turno;
	public $hor_ent;
	public $hor_sal;
	public $observado;

	# Metodo constructor
	function __construct() {
		$this->db_name = 'bioclock';
	}

	# Metodo destructor del objeto
	function __destruct() {
		unset($this);
	}

	// METODOS
	function __toString() {
		return "|| $this->codEmp || $this->fecha || $this->turno || $this->hor_ent || $this->hor_sal || $this->observado ||";
	}

	# Traer datos de un registro de asistencia
	public function get($sqlWhere = '', $tableName = '') {
		if($sqlWhere != '' && $tableName != '') {
			$this->query = "SELECT * FROM $tableName WHERE $sqlWhere";
			$this->get_results_from_query();
		}
		if(count($this->rows) == 1) {
			foreach ($this->rows[0] as $propiedad=>$valor) {
				$this->$propiedad = $valor;
			}
			$this->mensaje = 'Registro encontrado';
		} else {
			$this->mensaje = 'Registro no encontrado';
		}
	}

	// 	//TODO: Con Mauricio coordinar que tipo de dato sera la tolerancia, int [min]?
	// 	protected function checkTimeInterval($start, $end, $time, $tolIni = 0, $tolFin = 0) {
	// 		// Convert to timestamp
	// 		$startTs = strtotime($start) + $tolIni * 60;
	// 		$endTs = strtotime($end) + $tolFin * 60;
	// 		$userTs = strtotime($time);

	// 		// Check that user date is between start & end
	// 		return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
	// 	}

	# Modificar un registro de asistencia
	public function edit($reg_data=array(), $tableName = '') {
		foreach ($reg_data as $field=>$value) {
			$this->$field = $value;
		}

		// 		$regLikeDB = new AsistenciaNoLaboral();
		// 		$regLikeDB->get($codEmp, $fecha, $turno, $hor_sal);
		// 		if($this->codEmp != $regLikeDB->codEmp && $this->fecha != $regLikeDB->fecha && $this->turno !== $regLikeDB->turno && $this->hor_sal != $regLikeDB->hor_sal) {
		$this->query = "
		UPDATE $tableName
		SET hor_ent = '$this->hor_ent',
		hor_sal = '$this->hor_sal',
		observado = '$this->observado'
		WHERE codEmp = '$this->codEmp' AND fecha = STR_TO_DATE('$this->fecha', '%m/%d/%Y') AND turno = '$this->turno'";
		$this->execute_single_query();
		$this->mensaje = 'Registro modificado';
		// 		} else {
		// 			$this->mensaje = 'Registro no modificado';
		// 		}
	}

	# Eliminar un registro de asistencia
	public function delete($sqlWhere = '', $tableName = '') {
		$this->query = "DELETE * FROM $tableName WHERE $sqlWhere";
		$this->execute_single_query();
		$this->mensaje = 'Registro eliminado';
	}
}

class AsistenciaLaboral extends Asistencia {
	# Traer datos de un registro de asistencia
	public function get($codEmp = -1, $fecha = '', $turno = -1) {
		if($codEmp != -1 && $fecha != '' && $turno != -1) {
			$sqlWhere = "codEmp = '$codEmp' AND fecha = STR_TO_DATE('$fecha', '%m/%d/%Y') AND turno = '$turno'";
			parent::get($sqlWhere, 'asis_lab');
		}
	}

	# Crear un nuevo registro de asistencia
	public function set() {
		if(isset($this->codEmp) && isset($this->fecha) && isset($this->turno)) {
			$regLikeDB = new AsistenciaLaboral();
			$regLikeDB->get($this->codEmp, $this->fecha, $this->turno);
			if($this->codEmp != $regLikeDB->codEmp && $this->fecha != $regLikeDB->fecha && $this->turno !== $regLikeDB->turno) {
				$this->query = "
				INSERT INTO asis_lab
				(codEmp, fecha, turno, hor_ent, hor_sal, observado)
				VALUES
				('$this->codEmp', STR_TO_DATE('$this->fecha', '%m/%d/%Y'), '$this->turno', '$this->hor_ent', '$this->hor_sal', '$this->observado')";
				$this->execute_single_query();
				$this->mensaje = 'Registro agregado exitosamente';
			} else {
				$this->mensaje = 'El registro ya existe';
			}
		} else {
			$this->mensaje = 'El registro no ha sido agregado';
		}
	}

	# Modificar un registro de asistencia
	public function edit($reg_data = array()) {
		if(isset($reg_data['hor_ent']) && isset($reg_data['hor_sal']) && isset($reg_data['turno'])) {
			$tableName = 'asis_lab';
			$turno = Turnos::getTurno($reg_data['turno']); //TODO: Falta la interface con la tarea de turnos de Mauricio

			if (
			parent::checkTimeInterval($turno['ini'], $turno['ini'], $reg_data['hor_ent'], -$turno['tol_ini'], $turno['tol_ini']) &&
			parent::checkTimeInterval($turno['fin'], $turno['fin'], $reg_data['hor_sal'], -$turno['tol_fin'], $turno['tol_fin'])) {
				$reg_data['observado'] = false;
				parent::edit($reg_data, $tableName);
			}
		}
	}

	# Eliminar un registro de asistencia
	public function delete($codEmp = '', $fecha = '', $turno = '') {
		$sqlWhere = "codEmp = '$codEmp' AND fecha = STR_TO_DATE('$fecha', '%m/%d/%Y') AND turno = '$turno'";
		parent::delete($sqlWhere, 'asis_lab');
	}
}


class AsistenciaNoLaboral extends Asistencia {
	# Traer datos de un registro de asistencia
	public function get($codEmp = -1, $fecha = '', $turno = -1, $hor_ent = '') {
		if($codEmp != -1 && $fecha != '' && $turno != -1 && $hor_ent != '') {
			$sqlWhere = "codEmp = '$codEmp' AND STR_TO_DATE('$fecha', '%m/%d/%Y') AND turno = '$turno' AND hor_ent = '$hor_ent'";
			parent::get($sqlWhere, 'asis_no_lab');
		}
	}

	# Crear un nuevo registro de asistencia
	public function set() {
		if(isset($this->codEmp) && isset($this->fecha) && isset($this->turno) && isset($this->hor_ent)) {
			$regLikeDB = new AsistenciaNoLaboral();
			$regLikeDB->get($this->codEmp, $this->fecha, $this->turno, $this->hor_ent);
			if($this->codEmp != $regLikeDB->codEmp && $this->fecha != $regLikeDB->fecha && $this->turno !== $regLikeDB->turno && $this->hor_ent != $regLikeDB->hor_ent) {
				$this->query = "
				INSERT INTO asis_no_lab (codEmp, fecha, turno, hor_ent, hor_sal, observado) VALUES
				('$this->codEmp', STR_TO_DATE('$this->fecha', '%m/%d/%Y'), '$this->turno', '$this->hor_ent', '$this->hor_sal', '$this->observado')";
				$this->execute_single_query();
				$this->mensaje = 'Registro agregado exitosamente';
			} else {
				$this->mensaje = 'El registro ya existe';
			}
		} else {
			$this->mensaje = 'El registro no ha sido agregado';
		}
	}

	# Modificar un registro de asistencia
	public function edit($reg_data=array()) {
		$this->mensaje = 'El registro no ha sido modificado';
	}

	# Eliminar un registro de asistencia
	public function delete($codEmp = '', $fecha = '', $turno = '', $hor_ent = '') {
		$sqlWhere = "codEmp = '$codEmp' AND fecha = STR_TO_DATE('$fecha', '%m/%d/%Y') AND turno = '$turno' AND hor_ent = '$hor_ent'";
		parent::delete($sqlWhere, 'asis_no_lab');
	}
}

class SalidasIntermedias extends Asistencia {
	# Traer datos de un registro de asistencia
	public function get($codEmp = -1, $fecha = '', $turno = -1, $hor_sal = '') {
		if($codEmp != -1 && $fecha != '' && $turno != -1 && $hor_sal != '') {
			$sqlWhere = "codEmp = '$codEmp' AND fecha = STR_TO_DATE('$fecha', '%m/%d/%Y') AND turno = '$turno' AND hor_sal = '$hor_sal'";
			parent::get($sqlWhere, 'sal_inter');
		}
	}

	# Crear un nuevo registro de asistencia
	public function set() {
		if(isset($this->codEmp) && isset($this->fecha) && isset($this->turno) && isset($this->hor_sal)) {
			$regLikeDB = new SalidasIntermedias();
			$regLikeDB->get($this->codEmp, $this->fecha, $this->turno, $this->hor_sal);
			if($this->codEmp != $regLikeDB->codEmp && $this->fecha != $regLikeDB->fecha && $this->turno !== $regLikeDB->turno && $this->hor_sal != $regLikeDB->hor_sal) {
				$this->query = "
				INSERT INTO sal_inter
				(codEmp, fecha, turno, hor_ent, hor_sal, observado)
				VALUES
				('$this->codEmp', STR_TO_DATE('$this->fecha', '%m/%d/%Y'), '$this->turno', '$this->hor_ent', '$this->hor_sal', '$this->observado')";
				$this->execute_single_query();
				$this->mensaje = 'Registro agregado exitosamente';
			} else {
				$this->mensaje = 'El registro ya existe';
			}
		} else {
			$this->mensaje = 'El registro no ha sido agregado';
		}
	}

	# Modificar un registro de asistencia
	public function edit($reg_data=array()) {
		if (isset($reg_data['hor_ent']) && isset($reg_data['hor_sal']) && $reg_data['turno']) {
			$tableName = 'sal_inter';
			$turno = Turnos::getTurno($reg_data['turno']); //TODO: Falta la interface con la tarea de turnos de Mauricio

			if(
			parent::checkTimeInterval($turno['ini'], $turno['fin'], $reg_data['hor_sal'], $turno['tol_ini'], -$turno['tol_fin']) &&
			parent::checkTimeInterval($turno['ini'], $turno['fin'], $reg_data['hor_ent'], $turno['tol_ini'], -$turno['tol_fin']) &&
			(strtotime($reg_data['hor_sal']) < strtotime($reg_data['hor_ent']))) {
				$reg_data['observado'] = false;
				parent::edit($reg_data, $tableName);
			} else {
				$this->mensaje = 'Registro no modificado';
			}
		}
	}

	# Eliminar un registro de asistencia
	public function delete($codEmp = -1, $fecha = '', $turno = -1, $hor_sal = '') {
		if($codEmp != -1 && $fecha != '' && $turno != -1 && $hor_sal != '') {
			$sqlWhere = "codEmp = '$codEmp' AND fecha = STR_TO_DATE('$fecha', '%m/%d/%Y') AND turno = '$turno' AND hor_sal = '$hor_sal'";
			parent::delete($sqlWhere, 'sal_inter');
		}
	}
}


?>