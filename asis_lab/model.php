<?php

# Importar modelo de abstraccion de base de datos
require_once('../core/db_abstract_model.php');
require_once('../core/db_abstract_asis.php');
require_once('../core/time_utils.php');

class AsistenciaLaboral extends Asistencia {
	# Traer datos de un registro de asistencia
	public function get($codEmp = -1, $fecha = '', $turno = -1) {
		if($codEmp != -1 && $fecha != '' && $turno != -1) {
			// 			$sqlFormat = "codEmp='%s' AND fecha = STR_TO_DATE('%s', '%%m/%%d/%%Y') AND turno='%s'";
			$sqlFormat = "codEmp='%s' AND fecha = '%s' AND turno='%s'";
			$sqlWhere = sprintf($sqlFormat, $codEmp, $fecha, $turno);
			parent::get($sqlWhere, 'asis_lab');
		}
	}

	# Crear un nuevo registro de asistencia
	public function set($reg_data = array()) {
		foreach ($reg_data as $field=>$value) {
			$$field = $value;
		}

		if(isset($codEmp) && isset($fecha) && isset($turno)) {
			$regLikeDB = new AsistenciaLaboral();
			$regLikeDB->get($codEmp, $fecha, $turno);

			if(!$regLikeDB->codEmp) {
				// 						VALUES ('%s', STR_TO_DATE('%s', '%%m/%%d/%%Y'), '%s', '%s', '%s', '%s')";
				$sqlFormat = "INSERT INTO asis_lab (codEmp, fecha, turno, hor_ent, hor_sal, observado)
						VALUES ('%s', '%s', '%s', '%s', '%s', '%s')";
				$this->query = sprintf($sqlFormat, $codEmp, $fecha, $turno, $hor_ent, $hor_sal, $observado);
				$this->execute_single_query();
				$this->mensaje = 'Registro agregado exitosamente';
			} else {
				$this->mensaje = 'Error: El registro ya existe';
			}
		} else {
			$this->mensaje = 'Error: El registro no ha sido agregado';
		}
	}

	# Modificar un registro de asistencia
	public function edit($reg_data = array()) {
		$tableName = 'asis_lab';
		parent::edit($reg_data, $tableName);
	}

	# Eliminar un registro de asistencia
	public function delete($codEmp = '', $fecha = '', $turno = '') {
		// 		$sqlFormat = "codEmp='%s' AND fecha = STR_TO_DATE('%s', '%%m/%%d/%%Y') AND turno='%s'";
		$sqlFormat = "codEmp='%s' AND fecha='%s' AND turno='%s'";
		$sqlWhere = sprintf($sqlFormat, $codEmp, $fecha, $turno);
		parent::delete($sqlWhere, 'asis_lab');
		// 		$this->mensaje .= " ($codEmp, $fecha, $turno)";
	}

	public function getAll() {
		try {
			$this->query = "SELECT * FROM asis_lab";
			$this->get_results_from_query();
			$this->mensaje = count($this->rows) > 0 ? 'Registros recuperados con �xito' : 'No existen registros';
			return $this->rows;
		} catch (Exception $e) {
			$this->mensaje = 'Error: ' . $e->getMessage();
		}
	}
}

?>