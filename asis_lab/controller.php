<?php
require_once('constants.php');
require_once('model.php');
require_once('view.php');

function handler() {
	$event = VIEW_REPORT;
	$uri = $_SERVER['REQUEST_URI'];
	$peticiones = array(
			ADD,
			SET,
			GET,
			DELETE,
			EDIT,
			REPORT,
			VIEW_ADD,
			VIEW_SET,
			VIEW_GET,
			VIEW_DELETE,
			VIEW_REPORT,
			VIEW_EDIT);
	foreach ($peticiones as $peticion) {
		$uri_peticion = MODULO.'/'.$peticion.'/';
		if( strpos($uri, $uri_peticion) == true ) {
			$event = $peticion;
		}
	}

	$register_data = helper_user_data();
	$register = set_obj();

	switch ($event) {
		case ADD:
			$data = array(
			'codEmp'=>'',
			'fecha'=>date('Y-m-d'),
			'hor_ent'=>'hh:mm:ss',
			'hor_sal'=>'hh:mm:ss'
					);
					retornar_vista(VIEW_ADD, $data);
					break;
		case SET:
			$register_data['observado'] = false;
			$register->set($register_data);
			$msg = $register->mensaje;
			if (strpos($msg, 'Error') === false) {
				$data = $register->getAll();
				retornar_vista(VIEW_REPORT, $data, $msg);
			} else {
				retornar_vista(VIEW_ADD, $register_data, $msg);
			}
			break;
		case GET:
			$register->get($register_data['codEmp'], $register_data['fecha'], $register_data['turno']);
			$data = array(
					'codEmp'=>$register->codEmp,
					'fecha'=>$register->fecha,
					'turno'=>$register->turno,
					'hor_ent'=>$register->hor_ent,
					'hor_sal'=>$register->hor_sal,
					'observado'=>$register->observado
			);
			retornar_vista(VIEW_GET, $data);
			break;
		case DELETE:
			$register->delete($register_data['codEmp'], $register_data['fecha'], $register_data['turno']);
			$msg = $register->mensaje;
			$data = $register->getAll();
			retornar_vista(VIEW_DELETE, $data, $msg);
			break;
		case EDIT:
			$register_data['observado'] = false;
			$register->edit($register_data);
			$msg = $register->mensaje;
			if (strpos($msg, 'Error') === false) {
				$data = $register->getAll();
				retornar_vista(VIEW_REPORT, $data, $msg);
			} else {
				retornar_vista(VIEW_GET, $register_data, $msg);
			}
			break;
		case REPORT:
			$data = $register->getAll();
			retornar_vista(VIEW_REPORT, $data, $register->mensaje);
			break;
		default:
			retornar_vista($event);
	}
}

function set_obj() {
	$obj = new AsistenciaLaboral();
	return $obj;
}

function helper_user_data() {
	$data = array();
	if($_POST) {
		if(array_key_exists('codEmp', $_POST)) {
			$data['codEmp'] = htmlentities($_POST['codEmp']);
		}
		if(array_key_exists('fecha', $_POST)) {
			$data['fecha'] = htmlentities($_POST['fecha']);
		}
		if(array_key_exists('turno', $_POST)) {
			$data['turno'] = htmlentities($_POST['turno']);
		}
		if(array_key_exists('hor_ent', $_POST)) {
			$data['hor_ent'] = htmlentities($_POST['hor_ent']);
		}
		if(array_key_exists('hor_sal', $_POST)) {
			$data['hor_sal'] = htmlentities($_POST['hor_sal']);
		}
		// 		$data['codEmp'] = array_key_exists('codEmp', $_POST) ? htmlentities($_POST['codEmp'] : '';
		// 		$data['fecha'] = array_key_exists('fecha', $_POST) ? htmlentities($_POST['fecha'] : '';
		// 		$data['hor_ent'] = array_key_exists('hor_ent', $_POST) ? htmlentities($_POST['hor_ent'] : '';
		// 		$data['hor_sal'] = array_key_exists('hor_sal', $_POST) ? htmlentities($_POST['hor_sal'] : '';
	} else if($_GET) {
		if(array_key_exists('codEmp', $_GET)) {
			$data['codEmp'] = htmlentities($_GET['codEmp']);
		}
		if(array_key_exists('fecha', $_GET)) {
			$data['fecha'] = htmlentities($_GET['fecha']);
		}
		if(array_key_exists('turno', $_GET)) {
			$data['turno'] = htmlentities($_GET['turno']);
		}
	}
	return $data;
}
handler();
?>