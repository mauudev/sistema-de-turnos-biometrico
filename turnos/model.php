<?php

require_once('../core/db_abstract_model.php');

class Turno extends DBAbstractModel{
	public $id_turno;
	public $descrip;
	public $hora_ent;
	public $tol_ent;
	public $hora_sal;
	public $tol_sal;
	public $habil;
	public $fecha_creacion;
	public $fecha_modif;

	function __construct(){
		$this->db_name = 'ctrl_asis';
	}

	function __destruct() {
		unset($this);
	}

	public function get($id_turno = ''){
		if($id_turno != '') {
			$this->query = "SELECT * FROM turno WHERE id_turno = $id_turno";
			$this->get_results_from_query();
			if(count($this->rows) == 1) {
				foreach ($this->rows[0] as $propiedad=>$valor) {
					$this->$propiedad = $valor;
				}
				$this->mensaje = 'Turno encontrado';
			} else {
				$this->mensaje = 'Error: Turno no encontrado';
			}
		}
	}

	public function set($data=array()) {
		foreach($data as $campo => $valor)
			$$campo = $valor;

		try {
			$fecha_creacion = date('Y-m-d');
			$fecha_modif = date('Y-m-d');
			$this->query = "INSERT INTO turno(descrip, hora_ent, tol_ent, hora_sal, tol_sal, habil, fecha_creacion, fecha_modif)
			VALUES('$descrip', '$hora_ent', '$tol_ent','$hora_sal','$tol_sal',$habil, '$fecha_creacion', '$fecha_modif')";
			$this->execute_single_query();
			$this->mensaje = 'Turno registrado correctamente!';
		} catch (Exception $e) {
			$this->mensaje = sprintf("Error: Turno no registrado (%s)", $e->getMessage());
		}
	}

	public function edit($data=array()){
		if(array_key_exists('id_turno', $data)) {
			foreach($data as $field => $value)
				$$field = $value;
				
			try {
				$this->query = sprintf("UPDATE turno SET
						descrip = '%s',
						hora_ent = '%s',
						tol_ent = '%s',
						hora_sal = '%s',
						tol_sal = '%s',
						habil = %s,
						fecha_modif = '%s'
						WHERE id_turno = %d",
						$descrip,
						$hora_ent,
						$tol_ent,
						$hora_sal,
						$tol_sal,
						$habil,
						date('Y-m-d'),
						$id_turno);
				$this->execute_single_query();
				$this->mensaje = 'Turno modificado correctamente';
			} catch (Exception $e) {
				$this->mensaje = sprintf("Error: Turno no actualizado (%s)", $e->getMessage());
			}
		}
	}

	public function delete($id_turno='', $isActive=false){
		if($id_turno != '') {
			try {
				$this->query = sprintf("UPDATE turno SET
						habil = $isActive,
						fecha_modif = '%s'
						WHERE id_turno = %d",
						date('Y-m-d'),
						$id_turno);
				$this->execute_single_query();
				$this->mensaje = sprintf('Turno %s correctamente', $isActive ? 'activado' : 'desactivado');
			} catch (Exception $e) {
				$this->mensaje = sprintf("Error: No se pudo cambiar el estado del registros (%s)", $e->getMessage());
			}
		}
	}

	public function getAll($where = '') {
		try {
			$this->query = "SELECT * FROM turno $where";
			$this->get_results_from_query();
			$this->mensaje = count($this->rows) > 0 ? 'Registros recuperados con �xito' : 'No existen registros';
			return $this->rows;
		} catch (Exception $e) {
			$this->mensaje = "Error: " . $e->getMessage();
		}

	}

// 	public function getTurnos($ci) {
// 		try {
// 			//TODO: Falta completar con la tarea de turnos del sprint2 - codigo temporal
// 			$this->query = "SELECT * FROM turno WHERE id_turno=1 OR id_turno=2";
// 			// 			$this->query = "SELECT id_turno, descrip, hora_ent, tol_ent, hora_sal, tol_sal FROM turno, turno_emp WHERE turno_emp.id_turno = turno.id_turno AND turno_emp.cod_emp = $ci";
// 			$this->get_results_from_query();
// 			if (count($this->rows) == 0)
// 				throw new Exception('Error: Turnos no asignados a empleado');
// 			$this->mensaje = 'Registros recuperados con �xito';
// 			return $this->rows;
// 		} catch (Exception $e) {
// 			$this->mensaje = "Error: $e->getMessage()";
// 		}

// 	}
}

?>