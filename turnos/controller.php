<?php
require_once('constants.php');
require_once('model.php');
require_once('view.php');

function handler() {
	$event = VIEW_REPORT;
	$uri = $_SERVER['REQUEST_URI'];
	$peticiones = array(
			ADD,
			SET,
			GET,
			DELETE,
			EDIT,
			REPORT,
			VIEW_ADD,
			VIEW_SET,
			VIEW_GET,
			VIEW_DELETE,
			VIEW_REPORT,
			VIEW_EDIT);
	foreach ($peticiones as $peticion) {
		$uri_peticion = MODULO.'/'.$peticion.'/';
		if( strpos($uri, $uri_peticion) == true ) {
			$event = $peticion;
		}
	}

	$data = helper_data();
	$turno = set_obj();
	$turno->habil = $data['habil'];

	switch ($event) {
		case ADD:
			$data = array(
			'descrip'=>'',
			'hora_ent'=>'hh:mm:ss',
			'tol_ent'=>'',
			'hora_sal'=>'hh:mm:ss',
			'tol_sal'=>'',
			'habil'=>$turno->habil
			);
			retornar_vista(VIEW_ADD, $data, '', $turno->habil);
			break;
		case SET:
			$turno->set($data);
			$sqlWhere = sprintf("WHERE habil = '%s'", $data['habil']);
			$msg = $turno->mensaje;
			if (strpos($msg, 'Error') === false) {
				$data = $turno->getAll($sqlWhere);
				retornar_vista(VIEW_REPORT, $data, $msg, $turno->habil);
			} else {
				retornar_vista(VIEW_ADD, $data, $msg);
			}
			break;
		case GET:
			$turno->get($data['id_turno']);
			$data = array(
					'id_turno'=>$turno->id_turno,
					'descrip'=>$turno->descrip,
					'hora_ent'=>$turno->hora_ent,
					'tol_ent'=>$turno->tol_ent,
					'hora_sal'=>$turno->hora_sal,
					'tol_sal'=>$turno->tol_sal,
					'habil'=>$turno->habil,
					'fecha_creacion'=>$turno->fecha_creacion,
					'fecha_modif'=>$turno->fecha_modif
			);
			retornar_vista(VIEW_GET, $data, $turno->mensaje, $turno->habil);
			break;
		case DELETE:
			$turno->delete($data['id_turno'], $data['habil']);
			$msg = $turno->mensaje;
			$sqlWhere = sprintf("WHERE habil = '%s'", $data['habil']);
			$data = $turno->getAll($sqlWhere);
			retornar_vista(VIEW_REPORT, $data, $msg, $turno->habil);
			break;
		case EDIT:
			$turno->edit($data);
			$sqlWhere = sprintf("WHERE habil = '%s'", $data['habil']);
			$msg = $turno->mensaje;
			if (strpos($msg, 'Error') === false) {
				$data = $turno->getAll($sqlWhere);
				retornar_vista(VIEW_REPORT, $data, $msg, $turno->habil);
			} else {
				retornar_vista(VIEW_EDIT, array(), $msg);
			}
			break;
		case REPORT:
			$sqlWhere = sprintf("WHERE habil = '%s'", $data['habil']);
			$data = $turno->getAll($sqlWhere);
			retornar_vista(VIEW_REPORT, $data, $turno->mensaje, $turno->habil);
			break;
		default:
			retornar_vista($event);
	}
}

function set_obj() {
	$obj = new Turno();
	return $obj;
}

function helper_data() {
	$data = array();
	$data['habil'] = true;

	if($_POST) {
		if(array_key_exists('id_turno', $_POST))
			$data['id_turno'] = htmlentities($_POST['id_turno']);
		if(array_key_exists('descrip', $_POST))
			$data['descrip'] = htmlentities($_POST['descrip']);
		if(array_key_exists('hora_ent', $_POST))
			$data['hora_ent'] = htmlentities($_POST['hora_ent']);
		if(array_key_exists('tol_ent', $_POST))
			$data['tol_ent'] = htmlentities($_POST['tol_ent']);
		if(array_key_exists('hora_sal', $_POST))
			$data['hora_sal'] = htmlentities($_POST['hora_sal']);
		if(array_key_exists('tol_sal', $_POST))
			$data['tol_sal'] = htmlentities($_POST['tol_sal']);
		if(array_key_exists('habil', $_POST))
			$data['habil'] = htmlentities($_POST['habil']);
	} else if($_GET) {
		if(array_key_exists('id_turno', $_GET))
			$data['id_turno'] = htmlentities($_GET['id_turno']);
		if(array_key_exists('habil', $_GET))
			$data['habil'] =  htmlentities($_GET['habil']);
	}

	return $data;
}

handler();

?>
