<?php

$diccionario = array(
		'title'=>'Turnos',
		'subtitle'=>array(
				VIEW_ADD=>'Creaci�n de un nuevo turno',
				VIEW_SET=>'Creaci�n de un nuevo turno',
				VIEW_GET=>'Registro de turno',
				VIEW_DELETE=>'Eliminaci�n de turnos',
				VIEW_EDIT=>'Modificaci�n de turnos',
				VIEW_REPORT=>'Reporte de turnos'
		),
		'page_info'=>array(
				VIEW_ADD=>'Esta p�gina permite a�adir un turno',
				VIEW_SET=>'Esta p�gina permite a�adir un turno',
				VIEW_GET=>'Esta p�gina permite ver/editar un turno',
				VIEW_DELETE=>'Esta p�gina permite eliminar un turno',
				VIEW_EDIT=>'Esta p�gina permite modificar un turno',
				VIEW_REPORT=>'Esta p�gina permite visualizar los turnos existentes'
		),
		'content_title'=>array(
				VIEW_ADD=>'Formulario de adici�n de un turno',
				VIEW_SET=>'Formulario de adici�n de un turno',
				VIEW_GET=>'Formulario de edici�n de un turno',
				VIEW_EDIT=>'Formulario de edici�n de un turno',
				VIEW_DELETE=>'Formulario de eliminaci�n un turno',
				VIEW_REPORT=>'Turnos'
		),
		'links_menu'=>array(
				'VIEW_ADD'=>'/webs/sis2/'.MODULO.'/'.VIEW_ADD.'/',
				'VIEW_SET'=>'/webs/sis2/'.MODULO.'/'.VIEW_SET.'/',
				'VIEW_GET'=>'/webs/sis2/'.MODULO.'/'.VIEW_GET.'/',
				'VIEW_EDIT'=>'/webs/sis2/'.MODULO.'/'.VIEW_EDIT.'/',
				'VIEW_DELETE'=>'/webs/sis2/'.MODULO.'/'.VIEW_DELETE.'/',
				'VIEW_REPORT'=>'/webs/sis2/'.MODULO.'/'.VIEW_REPORT.'/'
		),
		'form_actions'=>array(
				'ADD'=>'/webs/sis2/'.MODULO.'/'.ADD.'/',
				'SET'=>'/webs/sis2/'.MODULO.'/'.SET.'/',
				'GET'=>'/webs/sis2/'.MODULO.'/'.GET.'/',
				'DELETE'=>'/webs/sis2/'.MODULO.'/'.DELETE.'/',
				'EDIT'=>'/webs/sis2/'.MODULO.'/'.EDIT.'/',
				'REPORT'=>'/webs/sis2/'.MODULO.'/'.REPORT.'/'
		)
);

function get_template($form='site_template') {
	$file = '../site_media/html/'.$form.'.html';
	$template = file_get_contents($file);
	return $template;
}

function render_dinamic_data($html, $data) {
	foreach ($data as $clave=>$valor) {
		$html = str_replace('{'.$clave.'}', $valor, $html);
	}
	return $html;
}

// function render_select_turnos($turno = 0, $readOnly = false) {
// 	$optStatus = $readOnly ? "disabled" : "";
// 	$r = "<select style='opacity: 0; width: 170px;' name='turno' id='turno' class='uniform' $optStatus>";

// 	$turnosDB = new Turno();
// 	$turnos = $turnosDB->getAll();
// 	for ($i = 1; $i < count($turnos); $i++) {
// 		$r .= sprintf("<option value='%s'>%s</option>", $turnos[$i]['id_turno'], $turnos[$i]['descrip']);
// 	}

// 	$r .= "</select>";
// 	$r .= sprintf("<script>document.getElementById('turno').value = %s;</script>",$turno);

// 	return $r;
// }

function render_data_table($rows) {
	// 	$html =
	// 	"<table border='0' class='display data-table' id='example'>
	// 			<thead>
	// 			<tr>
	// 			<th>Descripci�n</th>
	// 			<th>Entrada</th>
	// 			<th>Tolerancia Entrada</th>
	// 			<th>Salida</th>
	// 			<th>Tolerancia Salida</th>
	// 			<th>Fecha Modif</th>
	// 			<th>Fecha Creacion</th>
	// 			<th>Habilitado</th>
	// 			<th></th>
	// 			</tr>
	// 			</thead>
	// 			<tbody>";

	$html =
	"<table border='0' class='display data-table' id='example'>
			<thead>
			<tr>
			<th>Descripci�n</th>
			<th>Entrada</th>
			<th>Tolerancia Entrada</th>
			<th>Salida</th>
			<th>Tolerancia Salida</th>
			<th>Fecha Modif</th>
			<th>Fecha Creacion</th>
			<th></th>
			</tr>
			</thead>
			<tbody>";

	$turno = new Turno();

	foreach ($rows as $row) {
		$html .= "<tr class='gradeA'>";
		$html .= sprintf("<td class='center'>%s</td>", $row['descrip']);
		$html .= sprintf("<td class='center'>%s</td>", $row['hora_ent']);
		$html .= sprintf("<td class='center'>%s</td>", $row['tol_ent']);
		$html .= sprintf("<td class='center'>%s</td>", $row['hora_sal']);
		$html .= sprintf("<td class='center'>%s</td>", $row['tol_sal']);
		$html .= sprintf("<td class='center'>%s</td>", $row['fecha_modif']);
		$html .= sprintf("<td class='center'>%s</td>", $row['fecha_creacion']);
		// 		$html .= sprintf("<td class='center'>%s</td>", $row['habil'] ? '<span class="observation-good">habilitado</span>' : '<span class="observation-wrong">deshabilitado</span>');
		// 		$html .= sprintf("
		// 				<td>
		// 				<a href='{GET}?id_turno=%1\$d'>editar</a>&nbsp/
		// 				<a href='{DELETE}?id_turno=%1\$d'>eliminar</a>
		// 				</td>",
		// 				$row['id_turno']);

		if ($row['habil'] == true) {
			$html .= sprintf("
					<td><a href='{GET}?id_turno=%1\$d'>editar</a>&nbsp/
					<a href='{DELETE}?id_turno=%1\$d&habil=0'>desactivar</a>
					</td>",
					$row['id_turno']);
		} else {
			$html .= sprintf("
					<td><a href='{GET}?id_turno=%1\$d'>editar</a>&nbsp/
					<a href='{DELETE}?id_turno=%1\$d&habil=1'>activar</a>
					</td>",
					$row['id_turno']);
		}

		$html .= "</tr>";
	}

	$html .= "</tbody></table>";

	return $html;
}

function retornar_vista($vista, $data=array(), $msg = '', $active = true) {
	global $diccionario;

	$html = get_template();
	$html = str_replace('{page_content}', get_template(MODULO.'_'.$vista), $html);

	if ($vista == VIEW_REPORT) {
		$html = str_replace('{data_table}', render_data_table($data), $html);
		$tableTitle = sprintf("%s %s", $diccionario['content_title'][$vista], $active ? 'activos' : 'inactivos');
		$html = str_replace('{table_title}', $tableTitle, $html);
	} else {
		$html = str_replace('{form_title}', $diccionario['content_title'][$vista], $html);
		$html = render_dinamic_data($html, $data);

		// 		if (isset($data['turno'])) {
		// 			$html = str_replace('{turnos}', render_select_turnos($data['turno'], true), $html);
		// 		} else {
		// 			$html = str_replace('{turnos}', render_select_turnos(), $html);
		// 		}
	}

	$html = str_replace('{title}', $diccionario['title'], $html);
	$html = str_replace('{subtitle}', $diccionario['subtitle'][$vista],$html);
	$html = str_replace('{page_info}', $diccionario['page_info'][$vista],$html);

	$html = render_dinamic_data($html, $diccionario['form_actions']);
	$html = render_dinamic_data($html, $diccionario['links_menu']);
	$html = str_replace('{habil}', $active, $html);

	if ($msg == '') {
		$html = str_replace('{mensaje}', "<div id='box-msg' class='albox errorbox' style='visibility: hidden'></div>", $html);
	} else {
		if (strpos($msg, 'Error') === false ) {
			$msg = sprintf("<div id='box-msg' class='albox succesbox'>%s</div>", $msg);
		} else {
			$msg = sprintf("<div id='box-msg' class='albox errorbox'>%s</div>", $msg);
		}
		$html = str_replace('{mensaje}', $msg, $html);
	}

	print $html;
}

?>
