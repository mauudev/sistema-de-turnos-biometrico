<?php
require_once('constants.php');
require_once('model.php');
require_once('view.php');

function handler() {
	$event = VIEW_REPORT;
	$uri = $_SERVER['REQUEST_URI'];
	$peticiones = array(
			ADD,
			SET,
			GET,
			DELETE,
			EDIT,
			REPORT,
			VIEW_ADD,
			VIEW_SET,
			VIEW_GET,
			VIEW_DELETE,
			VIEW_REPORT,
			VIEW_EDIT);
	foreach ($peticiones as $peticion) {
		$uri_peticion = MODULO.'/'.$peticion.'/';
		if( strpos($uri, $uri_peticion) == true ) {
			$event = $peticion;
		}
	}

	$register_data = helper_user_data();
	$register = set_obj();

	switch ($event) {
		case ADD:
			$data = array(
			'codEmp'=>'',
			'fecha'=>date('Y-m-d'),
			'hor_ent'=>'hh:mm:ss',
			'hor_sal'=>'hh:mm:ss'
					);
					retornar_vista(VIEW_ADD, $data);
					break;
		case SET:
			$register_data['observado'] = false;
			$register->set($register_data);
			$msg = $register->mensaje;
			if (strpos($msg, 'Error') === false) {
				$data = $register->getAll();
				retornar_vista(VIEW_REPORT, $data, $msg);
			} else {
				retornar_vista(VIEW_ADD, $register_data, $msg);
			}
			break;
		case GET:
			$register->get($register_data['codEmp'],
			$register_data['fecha'],
			$register_data['turno'],
			$register_data['hor_ent']);
			$data = array(
					'codEmp'=>$register->codEmp,
					'fecha'=>$register->fecha,
					'turno'=>$register->turno,
					'hor_ent'=>$register->hor_ent,
					'hor_sal'=>$register->hor_sal,
					'observado'=>$register->observado
			);
			retornar_vista(VIEW_GET, $data);
			break;
		case DELETE:
			$register->delete($register_data['codEmp'],
			$register_data['fecha'],
			$register_data['turno'],
			$register_data['hor_ent']);
			$msg = $register->mensaje;
			$data = $register->getAll();
			retornar_vista(VIEW_DELETE, $data, $msg);
			break;
		case EDIT:
			$register_data['observado'] = false;
			$register->edit($register_data);
			$msg = $register->mensaje;
			if (strpos($msg, 'Error') === false) {
				$data = $register->getAll();
				retornar_vista(VIEW_REPORT, $data, $msg);
			} else {
				retornar_vista(VIEW_GET, $register_data, $msg);
			}
			break;
		case REPORT:
			$data = $register->getAll();
			retornar_vista(VIEW_REPORT, $data, $register->mensaje);
			break;
		default:
			retornar_vista($event);
	}
}

function set_obj() {
	$obj = new AsistenciaNoLaboral();
	return $obj;
}

function helper_user_data() {
	$area_data = array();
	if($_POST) {
		if(array_key_exists('codEmp', $_POST)) {
			$area_data['codEmp'] = htmlentities($_POST['codEmp']);
		}
		if(array_key_exists('fecha', $_POST)) {
			$area_data['fecha'] = htmlentities($_POST['fecha']);
		}
		if(array_key_exists('turno', $_POST)) {
			$area_data['turno'] = htmlentities($_POST['turno']);
		}
		if(array_key_exists('hor_ent', $_POST)) {
			$area_data['hor_ent'] = htmlentities($_POST['hor_ent']);
		}
		if(array_key_exists('hor_sal', $_POST)) {
			$area_data['hor_sal'] = htmlentities($_POST['hor_sal']);
		}
	} else if($_GET) {
		if(array_key_exists('codEmp', $_GET)) {
			$area_data['codEmp'] = htmlentities($_GET['codEmp']);
		}
		if(array_key_exists('fecha', $_GET)) {
			$area_data['fecha'] = htmlentities($_GET['fecha']);
		}
		if(array_key_exists('turno', $_GET)) {
			$area_data['turno'] = htmlentities($_GET['turno']);
		}
		if(array_key_exists('hor_ent', $_GET)) {
			$area_data['hor_ent'] = htmlentities($_GET['hor_ent']);
		}
	}
	return $area_data;
}
handler();
?>